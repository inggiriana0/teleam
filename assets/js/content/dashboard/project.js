$(function () {
 
//=======================//
//table_salesplan//
    var table_acctreat = $('#table_acctreat').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "month" },
        // {
        //   "data": "active", render: function (data, type, row, meta) {
        //     return meta.settings.fnFormatNumber(row.active);
        //   }
        // },
        { "data": "active",  render: $.fn.dataTable.render.number('.')},
        // { "data": "active",  render: $.fn.dataTable.render.number( ',', '.', 3, 'Rp' )},
        { "data": "revenue",  render: $.fn.dataTable.render.number('.')}
      ],
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $(nRow).find('td:eq(0)').css('background-color', 'powderblue');
          if (aData.month == "TOTAL") {
            //cell background color
            $(nRow).find('td:eq(1)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(2)').css('background-color', 'powderblue');
          }
      },
      "ajax":{
        url :base_url+'dashboard/get_dashboard_project', // json datasource
        type: "post",
        data: function(d){
          d.id_project = '1';
          // d.searchby_name = $('#searchby_name').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
  //table_salesplan//
    var table_salesplan = $('#table_salesplan').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "month" },
        { "data": "active",  render: $.fn.dataTable.render.number('.')},
        { "data": "revenue",  render: $.fn.dataTable.render.number('.')}
      ],
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $(nRow).find('td:eq(0)').css('background-color', 'powderblue');
          if (aData.month == "TOTAL") {
            //cell background color
            $(nRow).find('td:eq(1)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(2)').css('background-color', 'powderblue');
          }
      },
      "ajax":{
        url :base_url+'dashboard/get_dashboard_project', // json datasource
        type: "post",
        data: function(d){
          d.id_project = '2';
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
  //table_education//
    var table_education = $('#table_education').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "month" },
        { "data": "active",  render: $.fn.dataTable.render.number('.')},
        { "data": "revenue",  render: $.fn.dataTable.render.number('.')}
      ],
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $(nRow).find('td:eq(0)').css('background-color', 'powderblue');
          if (aData.month == "TOTAL") {
            //cell background color
            $(nRow).find('td:eq(1)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(2)').css('background-color', 'powderblue');
          }
      },
      "ajax":{
        url :base_url+'dashboard/get_dashboard_project', // json datasource
        type: "post",
        data: function(d){
          d.id_project = '3';
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
  //table_health//
    var table_health = $('#table_health').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "month" },
        { "data": "active",  render: $.fn.dataTable.render.number('.')},
        { "data": "revenue",  render: $.fn.dataTable.render.number('.')}
      ],
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $(nRow).find('td:eq(0)').css('background-color', 'powderblue');
          if (aData.month == "TOTAL") {
            //cell background color
            $(nRow).find('td:eq(1)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(2)').css('background-color', 'powderblue');
          }
      },
      "ajax":{
        url :base_url+'dashboard/get_dashboard_project', // json datasource
        type: "post",
        data: function(d){
          d.id_project = '4';
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
//=======================//

})
