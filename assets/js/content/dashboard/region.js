$(function () {
 
//=======================//
//table_region//
    var table_region = $('#table_region').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "region" },
        { "data": "total" },
        { "data": "januari" },
        { "data": "februari" },
        { "data": "maret" },
        { "data": "april" },
        { "data": "mei" },
        { "data": "juni" },
        { "data": "juli" },
        { "data": "agustus" },
        { "data": "september" },
        { "data": "oktober" },
        { "data": "november" },
        { "data": "desember" }
      ],
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          // $(nRow).find('td:eq(0)').css('background-color', 'powderblue');
          if (aData.region.match(/^.*TOTAL.*$/))  {
          // if (aData.region == "TOTAL") {
            //cell background color
            $(nRow).find('td:eq(0)').css('background-color', 'powderblue',);
            $(nRow).find('td:eq(1)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(2)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(3)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(4)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(5)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(6)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(7)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(8)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(9)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(10)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(11)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(12)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(13)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(0)').css('font-weight', 'bold');
            $(nRow).find('td:eq(1)').css('font-weight', 'bold');
            $(nRow).find('td:eq(2)').css('font-weight', 'bold');
            $(nRow).find('td:eq(3)').css('font-weight', 'bold');
            $(nRow).find('td:eq(4)').css('font-weight', 'bold');
            $(nRow).find('td:eq(5)').css('font-weight', 'bold');
            $(nRow).find('td:eq(6)').css('font-weight', 'bold');
            $(nRow).find('td:eq(7)').css('font-weight', 'bold');
            $(nRow).find('td:eq(8)').css('font-weight', 'bold');
            $(nRow).find('td:eq(9)').css('font-weight', 'bold');
            $(nRow).find('td:eq(10)').css('font-weight', 'bold');
            $(nRow).find('td:eq(11)').css('font-weight', 'bold');
            $(nRow).find('td:eq(12)').css('font-weight', 'bold');
            $(nRow).find('td:eq(13)').css('font-weight', 'bold');
          }
      },
      "ajax":{
        url :base_url+'c_admin/get_dashboard_region', // json datasource
        type: "post",
        data: function(d){
          d.searchby_id_project = '1';
          // d.searchby_name = $('#searchby_name').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
//table_region2//
    var table_region2 = $('#table_region2').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "region" },
        { "data": "total" },
        { "data": "januari" },
        { "data": "februari" },
        { "data": "maret" },
        { "data": "april" },
        { "data": "mei" },
        { "data": "juni" },
        { "data": "juli" },
        { "data": "agustus" },
        { "data": "september" },
        { "data": "oktober" },
        { "data": "november" },
        { "data": "desember" }
      ],
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          // $(nRow).find('td:eq(0)').css('background-color', 'powderblue');
          if (aData.region.match(/^.*TOTAL.*$/))  {
          // if (aData.region == "TOTAL") {
            //cell background color
            $(nRow).find('td:eq(0)').css('background-color', 'powderblue',);
            $(nRow).find('td:eq(1)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(2)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(3)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(4)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(5)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(6)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(7)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(8)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(9)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(10)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(11)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(12)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(13)').css('background-color', 'powderblue');
            $(nRow).find('td:eq(0)').css('font-weight', 'bold');
            $(nRow).find('td:eq(1)').css('font-weight', 'bold');
            $(nRow).find('td:eq(2)').css('font-weight', 'bold');
            $(nRow).find('td:eq(3)').css('font-weight', 'bold');
            $(nRow).find('td:eq(4)').css('font-weight', 'bold');
            $(nRow).find('td:eq(5)').css('font-weight', 'bold');
            $(nRow).find('td:eq(6)').css('font-weight', 'bold');
            $(nRow).find('td:eq(7)').css('font-weight', 'bold');
            $(nRow).find('td:eq(8)').css('font-weight', 'bold');
            $(nRow).find('td:eq(9)').css('font-weight', 'bold');
            $(nRow).find('td:eq(10)').css('font-weight', 'bold');
            $(nRow).find('td:eq(11)').css('font-weight', 'bold');
            $(nRow).find('td:eq(12)').css('font-weight', 'bold');
            $(nRow).find('td:eq(13)').css('font-weight', 'bold');
          }
      },
      "ajax":{
        url :base_url+'c_admin/get_dashboard_region', // json datasource
        type: "post",
        data: function(d){
          d.searchby_id_project = '2';
          // d.searchby_name = $('#searchby_name').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
//=======================//

})
