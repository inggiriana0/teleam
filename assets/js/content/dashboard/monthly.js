$(function () {
 
//=======================//
//datatable//
    var table_monthly = $('#table_monthly').DataTable({
      "searching": false,
      "paging": false,
      "ordering": false,
      "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "bInfo" : false,
      // "pageLength": 100,
      // "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "bln" },
        { "data": "agent" },
        { "data": "call" },
        { "data": "contacted" },
        { "data": "agree" },
        { "data": "active" },
        { "data": "revenue" },
        { "data": "rev_mom" },
        { "data": "contacted_rate" },
        { "data": "conversion_rate" },
        { "data": "conversion_rate_psb" },
        { "data": "conversion_rate_uplift" },
        { "data": "active_rate" },
        { "data": "target" },
        { "data": "ach" }
      ],
      "ajax":{
        url :base_url+'dashboard/get_dashboard_monthly', // json datasource
        type: "post",
        data: function(d){
          // d.searchby_nik_csdm = $('#searchby_nik_csdm').val();
          // d.searchby_name = $('#searchby_name').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
//=======================//

// --------  auto scroll table  ------------------------------------------------------------//
  var $el = $(".table-autoscroll");
  function anim() {
    var st = $el.scrollTop();
    var sb = $el.prop("scrollHeight")-$el.innerHeight();
    $el.animate({scrollTop: st<sb/2 ? sb : 0}, 15000, anim);
  }
  function stop(){
    $el.stop();
  }
  anim();
  $el.hover(stop, anim);
// -----------------------------------------------------------------------------------------//

// --------  chart  ----------------------------------------------------------------------//
  'use strict';
  
  $.ajax({
      url: base_url+'dashboard/get_dashboard_monthly_for_chart',
      type: "post",
      dataType: 'json',
      success: function(data){
          var datalabel = [];
          var datarevenue = [];
          var datatarget = [];

          for (var i = 0; i < Object.keys(data['data']).length; i++) {
            datalabel.push( data['data'][i]['label'] );
            datarevenue.push( data['data'][i]['revenue'] );
            datatarget.push( data['data'][i]['target'] );
          }

          data_chart(datalabel, datarevenue, datatarget);

      },
      error: function(error_data){
          console.log("Error data");
      }
  });
  
  function data_chart(datalabel, datarevenue, datatarget){
    // console.log(datalabel);
    // console.log(datarevenue);
    // console.log(datatarget);

    var numberWithCommas = function(x) {
       return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };

    var areaChartData = 
    {
      labels  : datalabel,
      datasets: 
        [
          {
            label               : 'Target',
            backgroundColor     : 'rgba(60,141,188,0.9)',
            borderColor         : 'rgba(60,141,188,0.8)',
            pointRadius         : false,
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : datatarget
          },
          {
            label               : 'Revenue',
            backgroundColor     : '#f39c12',
            borderColor         : '#f39c12',
            pointRadius         : false,
            pointColor          : '#f39c12',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: '#c1c7d1',
            data                : datarevenue
          },
          // {
          //   label               : "ach %",
          //   type                : "line",
          //   backgroundColor     : 'yellow',
          //   borderColor         : "yellow",
          //   pointBorderColor    : "black",
          //   fill                : false,
          //   // datalabels          : {color: 'black'},
          //   data                : [45000000, 50000000, 65000000, 52000000, 30000000, 70000000, 60000000, 48000000, 54000000, 56000000, 75000000, 63000000, 58000000, 20000000]
          // },  
        ]
    }

    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    var title_target = areaChartData.datasets[0]
    var tilte_revenue = areaChartData.datasets[1]
    // var temp2 = areaChartData.datasets[2]
    barChartData.datasets[0] = tilte_revenue
    barChartData.datasets[1] = title_target
    // barChartData.datasets[2] = temp2

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false,
      // events                  : false,
      // tooltips                : {
      //                               enabled: false
      //                           },
      // hover                   : {
      //                               animationDuration: 0
      //                           },
      // animation               : {
      //                             duration: 1,
      //                             onComplete: function () {
      //                                 var chartInstance = this.chart,
      //                                     ctx = chartInstance.ctx;
      //                                 ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
      //                                 ctx.textAlign = 'center';
      //                                 ctx.textBaseline = 'bottom';

      //                                 this.data.datasets.forEach(function (dataset, i) {
      //                                     var meta = chartInstance.controller.getDatasetMeta(i);
      //                                     meta.data.forEach(function (bar, index) {
      //                                         var data = dataset.data[index];                            
      //                                         ctx.fillText(data, bar._model.x, bar._model.y - 5);
      //                                     });
      //                                 });
      //                             }
      //                           },
      // animation                 : {
      //                               duration: 500,
      //                               easing: "easeOutQuart",
      //                               onComplete: function () {
      //                                   var ctx = this.chart.ctx;
      //                                   ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
      //                                   ctx.textAlign = 'center';
      //                                   ctx.textBaseline = 'bottom';

      //                                   this.data.datasets.forEach(function (dataset) {
      //                                       for (var i = 0; i < dataset.data.length; i++) {
      //                                           var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
      //                                               scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
      //                                           ctx.fillStyle = '#444';
      //                                           var y_pos = model.y - 5;
      //                                           // Make sure data value does not get overflown and hidden
      //                                           // when the bar's value is too close to max value of scale
      //                                           // Note: The y value is reverse, it counts from top down
      //                                           if ((scale_max - model.y) / scale_max >= 0.93)
      //                                               y_pos = model.y + 20; 
      //                                           ctx.fillText(dataset.data[i], model.x, y_pos);
      //                                       }
      //                                   });               
      //                               }
      //                           },
      tooltips: {
           mode: 'label',
           callbacks: {
              label: function(tooltipItem, data) {
                 return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
              }
           }
        },
      scales: {
         // xAxes: [{
         //    stacked: true,
         //    gridLines: {
         //       display: false
         //    },
         // }],
         yAxes: [{
            // stacked: true,
            ticks: {
               beginAtZero: true,
               callback: function(value) {
                  return numberWithCommas(value);
               },
            },
         }],
      }, // scales
      // legend: {
      //    display: true
      // },
      plugins: {
         datalabels: {
            display: true,
            align: 'center',
            anchor: 'center'
         }
      },
      // scales                  : {
      //                             yAxes: [{
      //                               ticks: {
      //                                 beginAtZero: true,
      //                               }
      //                             }]
      //                           },
      // plugins: {
      //   datalabels: {
      //     display: true,
      //     color: 'blue',
      //     anchor: 'end',
      //     align: 'top',
      //     // and if you need to format how the value is displayed...
      //     // formatter: function(value, context) {
      //     //     return GetValueFormatted(value);
      //     // },
      //     // formatter: Math.round,
      //     font: {
      //       weight: 'bold'
      //     }
      //   }
      // },
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar', 
      data: barChartData,
      options: barChartOptions
    });

    // --------  save chart  -------------------------------------------------------------------//
      var date2 = (new Date()).toISOString().split('T')[0];
      // document.getElementById('date').innerHTML = date;

      document.getElementById('btn-download').onclick = function() {
        // Trigger the download
        var a = document.createElement('a');
        a.href = barChart.toBase64Image();
        a.download = 'daily chart '+date2+'.png';
        // a.download = 'daily chart '+date2+'.jpg';
        a.click();
      }
  }
// -----------------------------------------------------------------------------------------//


})
