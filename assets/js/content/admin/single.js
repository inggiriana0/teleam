$(function () {

  //=======================//
  //Setting datepicker
    // $('#tgl_cari').datepicker({
    //   autoclose: true
    // });
    $('#tgl_cari').daterangepicker({
      dateLimit: {
          'months': 1
      }
    });
  //=======================//

  //=======================//
  //single datatable//
    var table_single = $('#table_single').DataTable({
      "searching": false,
      // "paging": true,
      // "ordering": true,
      // "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "pageLength": 10,
      "order": [[ 0, "asc" ]],
      "columns": [
        { "data": "created" },
        { "data": "msisdn" },
        { "data": "jenis_wl" },
        { "data": "tgl_call" },
        { "data": "tgl_aktif" },
        { "data": "name_status_call" },
        { 
            "data": "id",
            render : function(data, type, row, meta) 
            { return "<button type='button' data-id_single='"+data+"' class='btn btn-info btn-sm' title='View'><i class='fas fa-eye'></i></button>"} 
        },
      ],
      "ajax":{
        url :base_url+'c_single/get_single', // json datasource
        type: "post",
        data: function(d){
          d.tgl_cari = $('#tgl_cari').val();
          // d.date_end = $('#tgl_cari').val();
          // d.csdm = $('#csdm').val();
          // d.msisdn = $('#msisdn5').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
  //button_filter single
    $('#button_filter').click(function(){
      table_single.ajax.reload();
    });
  //export excel single
    $('#export').click(function(){
      var date = $('#tgl_cari').val();
      var date0 = date.split(" - ");
      var date_start = date0[0];
      var date_end = date0[1];
      // console.log(date_start);
      // console.log(date_end);

      // var msisdn = $('#msisdn2').val();
      // var csdm = $('#csdm').val();

      if(typeof date_start == 'undefined' || date_start =='' || date_start == null) { date_start = '-' }
      if(typeof date_end == 'undefined' || date_end =='' || date_end == null) { date_end = '-' }  
      // if(typeof msisdn == 'undefined' || msisdn =='' || msisdn == null) { msisdn = '-' }  
      // if(typeof csdm == 'undefined' || csdm =='' || csdm == null) { csdm = '-' }  
      var url = base_url+'c_single/get_xls/'+date_start+'/'+date_end;
      window.open(url);
    });
  //single view button
    $('#table_single tbody').on('click', 'button', function(){
      var id_single = $(this).data('id_single');
      jQuery.ajax({
        url: base_url+'c_single/get_single_byid',
        type: 'POST',
        data: {id_single:id_single},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
          // console.log(data);
          document.getElementById("card_form").style.display = "block";

          $("#id_single").val(data['id']);
          $("#nik_csdm").val(data['nik_csdm']);
          $("#name").val(data['name']);
          $("#leader").val(data['leader']);
          $("#site").val(data['site']);
          $("#jenis_wl").val(data['jenis_wl']);
          $("#tgl_call").val(data['tgl_call']);
          $("#jam_call").val(data['jam_call']);
          $("#tgl_aktif").val(data['tgl_aktif']);
          $("#msisdn").val(data['msisdn']);
          $("#nama_pelanggan_dsc").val(data['nama_pelanggan_dsc']);
          $("#nama_akun").val(data['nama_akun']);
          $("#prt").val(data['prt']);
          $("#los").val(data['los']);
          $("#arpu").val(data['arpu']);
          $("#status_call").val(data['name_status_call']);
          $("#reason_call").val(data['name_reason_call']);
          $("#subreason_call").val(data['name_subreason_call']);
          $("#paket_sebelumnya").val(data['paket_sebelumnya']);
          $("#paket_aktif").val(data['paket_aktif']);
          $("#revenue").val(data['revenue']);
          $("#delta_revenue").val(data['delta_revenue']);
          $("#wl_source").val(data['wl_source']);
          $("#region").val(data['region']);
          $("#area").val(data['area']);
          $("#keterangan").val(data['keterangan']); 
          $("#created").val(data['created']);  
          $("#last_edited").val(data['last_edited']);
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log(textStatus.reponseText);
        }
      });
    })
  //=======================//


});