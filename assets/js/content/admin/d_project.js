$(function () {
	
	// $('.select2_project').select2({});

	//=======================//
	//get_dashboard_project//
		function get_dashboard_project(){
			jQuery.ajax({
				url: base_url+'dashboard/get_dashboard_project',
				type: 'POST',
				data: {id_project:$('#id_project').val()},
				dataType : 'json',
				success: function(data, textStatus, xhr) {
					// var str = '';
					var str = "<script type='text/javascript'>$('.money2').mask('000.000.000.000.000.000', {reverse: true});</script>";
					$("#listproject").html('');
					// var id_data = [];
					for (var i = 0; i < Object.keys(data['data']).length; i++) {
						if (data['data'][i]['month'] != 'TOTAL') {
							str += '<div class="form-group-sm row"><div class="col-sm-2"><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text form-control form-control-sm text-sm">Month</span></div><input type="text" class="form-control form-control-sm text-sm" value="'+data['data'][i]['month']+'" id="month" data-id_data="'+data['data'][i]['id']+'"></div></div><div class="col-sm-3"><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text form-control form-control-sm text-sm">Active</span></div><input type="text" class="form-control form-control-sm text-sm money2" value="'+data['data'][i]['active']+'" id="active" data-id_data="'+data['data'][i]['id']+'"></div></div><div class="col-sm-3"><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text form-control form-control-sm text-sm">revenue</span></div><input type="text" class="form-control form-control-sm text-sm money2" value="'+data['data'][i]['revenue']+'" id="revenue" data-id_data="'+data['data'][i]['id']+'"></div></div><div class="col-sm-4" style="visibility: hidden;"><input type="text" class="form-control form-control-sm text-sm" value="'+data['data'][i]['id']+'" id="id" data-id_data="'+data['data'][i]['id']+'"></div></div>';
						}
						if (data['data'][i]['month'] == 'TOTAL') {
							$('#month').val(data['data'][i]['month']);
							// $('#total_active').val(data['data'][i]['active']);
							// $('#total_revenue').val(data['data'][i]['revenue']);
							$('#id_total').val(data['data'][i]['id']);
						}
						// id_data.push( data['data'][i]['id'] );
					}
					$('#listproject').append(str);
					// data_project(id_data);

					$('#total_active').val(data['total_active']);
					$('#total_revenue').val(data['total_revenue']);
					$('#last_edited').val(data['last_edited']);
					$('#edited_by').val(data['edited_by']);
					
				},
				error: function(xhr, textStatus, errorThrown) {
					console.log(textStatus.reponseText);
				}
			});
		}
		jQuery.ajax({
			url: base_url+'dashboard/get_dashboard_project',
			type: 'POST',
			data: {id_project:$('#id_project').val()},
			dataType : 'json',
			success: function(data, textStatus, xhr) {
				var str = '';
				var str = "<script type='text/javascript'>$('.money2').mask('000.000.000.000.000.000', {reverse: true});</script>";
				$("#listproject").html('');
				// var id_data = [];
				for (var i = 0; i < Object.keys(data['data']).length; i++) {
					if (data['data'][i]['month'] != 'TOTAL') {
						str += '<div class="form-group-sm row"><div class="col-sm-2"><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text form-control form-control-sm text-sm">Month</span></div><input type="text" class="form-control form-control-sm text-sm" value="'+data['data'][i]['month']+'" id="month" data-id_data="'+data['data'][i]['id']+'"></div></div><div class="col-sm-3"><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text form-control form-control-sm text-sm">Active</span></div><input type="text" class="form-control form-control-sm text-sm money2" value="'+data['data'][i]['active']+'" id="active" data-id_data="'+data['data'][i]['id']+'"></div></div><div class="col-sm-3"><div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text form-control form-control-sm text-sm">revenue</span></div><input type="text" class="form-control form-control-sm text-sm money2" value="'+data['data'][i]['revenue']+'" id="revenue" data-id_data="'+data['data'][i]['id']+'"></div></div><div class="col-sm-4" style="visibility: hidden;"><input type="text" class="form-control form-control-sm text-sm" value="'+data['data'][i]['id']+'" id="id" data-id_data="'+data['data'][i]['id']+'"></div></div>';
					}
					if (data['data'][i]['month'] == 'TOTAL') {
						$('#month').val(data['data'][i]['month']);
						// $('#total_active').val(data['data'][i]['active']);
						// $('#total_revenue').val(data['data'][i]['revenue']);
						$('#id_total').val(data['data'][i]['id']);
					}
					// id_data.push( data['data'][i]['id'] );
				}
				$('#listproject').append(str);
				// data_project(id_data);

				$('#total_active').val(data['total_active']);
				$('#total_revenue').val(data['total_revenue']);
				$('#last_edited').val(data['last_edited']);
				$('#edited_by').val(data['edited_by']);
				
			},
			error: function(xhr, textStatus, errorThrown) {
				console.log(textStatus.reponseText);
			}
		});
	//button load data project//
		// $('#button_load_data').click(function () {
		// 	get_dashboard_project();
		// });
		$('#id_project').change(function () {
			// var column = $(this).attr('id');
			// console.log(column);
			// var x = document.getElementById("tes");
		 //  	x.value = x.value.toUpperCase();
			get_dashboard_project();
		});
	//save onchange//
		// function data_project(id_data){
		// 	console.log(id_data);
		// }
		$('#project div').on('change', 'input', function(){
			var id_data = $(this).data('id_data');
			var column = $(this).attr('id');
			var valuewithkoma = $(this).val();
			// var value = valuewithkoma.replace(".", "");
			// var value = valuewithkoma.replace(/[.]/g,'');
			var value = valuewithkoma.replace(/\./g,'');
			var id_project = $('#id_project').val();
			var temp = $(this).prev('input').val();
			if(valuewithkoma != temp){
		 		// toastr.success(column);
		 		// toastr.error(column);
		 		// toastr.info(column);
		 		// toastr.warning(column);
				// console.log(value);
				jQuery.ajax({
					url: base_url+'c_admin/do_update_dashboard_project',
					type: 'POST',
					data: {id:id_data, column:column, value:value, id_project:id_project},
					dataType : 'json',
					success: function(data, textStatus, xhr) {
						if(data['hasil'] == 'success'){
							$('#total_active').val(data['total_active']);
							$('#total_revenue').val(data['total_revenue']);
							$('#last_edited').val(data['last_edited']);
							$('#edited_by').val(data['edited_by']);
		 					toastr.success(data['message']);
						}
						else{
		 					toastr.error(data['message']);
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log(textStatus.reponseText);
					}
				});
			}
		})
	//=======================//

});

