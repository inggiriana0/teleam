$(function () {

  //=======================//
  //table_user datatable//
    var table_user = $('#table_user').DataTable({
      "searching": false,
      // "paging": true,
      // "ordering": true,
      // "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "pageLength": 10,
      "order": [[ 1, "asc" ]],
      "columns": [
        { "data": "nik_csdm" },
        { "data": "name" },
        { "data": "site" },
        { "data": "user_level" },
        { "data": "last_login" },
        { 
            "data": "id",
            render : function(data, type, row, meta) 
            { return "<button type='button' data-id_tb_user='"+data+"' class='btn btn-info btn-sm' title='View'><i class='fas fa-pencil-alt'></i></button>"} 
        },
      ],
      "ajax":{
        url :base_url+'c_admin/get_tb_user', // json datasource
        type: "post",
        data: function(d){
          d.searchby_nik_csdm = $('#searchby_nik_csdm').val();
          d.searchby_name = $('#searchby_name').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
  //button_filter single
    $('#button_filter').click(function(){
      table_user.ajax.reload();
    });
  //export excel single
    $('#export').click(function(){
      var searchby_nik_csdm = $('#searchby_nik_csdm').val();
      var searchby_name = $('#searchby_name').val();

      if(typeof searchby_nik_csdm == 'undefined' || searchby_nik_csdm =='' || searchby_nik_csdm == null) { searchby_nik_csdm = '-' }
      if(typeof searchby_name == 'undefined' || searchby_name =='' || searchby_name == null) { searchby_name = '-' }
      var url = base_url+'c_admin/get_xls/'+searchby_nik_csdm+'/'+searchby_name;
      window.open(url);
    });
  //single view button
    $('#table_user tbody').on('click', 'button', function(){
      var id_tb_user = $(this).data('id_tb_user');
      jQuery.ajax({
        url: base_url+'c_admin/get_tb_user_byid',
        type: 'POST',
        data: {id_tb_user:id_tb_user},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
          // console.log(data);
          document.getElementById("card_form").style.display = "block";
          $('#card_form').removeClass('card-primary');
          $('#card_form').addClass('card-success');
          $("#card_title").html('EDIT USER FORM');
          $('#button_save').removeClass('btn-primary');
          $('#button_save').addClass('btn-success');

          $("#id_tb_user").val(data['id']);
          $("#nik_csdm").val(data['nik_csdm']);
          $("#name").val(data['name']);
          $("#leader").val(data['leader']);
          $("#jabatan").val(data['jabatan']);
          $("#site").val(data['site']);
          $("#gender").val(data['gender']);
          $("#user_level").val(data['user_level']);
          $("#last_login").val(data['last_login']);
          $("#created").val(data['created']);
          $("#input_by").val(data['input_by']);
          $("#last_edited").val(data['last_edited']);
          $("#edited_by").val(data['edited_by']);
          $("#status_user").val(data['status_user']);

          //scroll to the top of the document
          document.documentElement.scrollTop = 0;
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log(textStatus.reponseText);
        }
      });
    })
  //user form (insert,edit)//
    $('#userdata_form').submit(function(e){
      e.preventDefault();
      $.ajax({
        url : base_url + 'c_admin/do_insert',
        type : 'POST',
        data : $(this).serialize(),
        dataType : 'json',
        success : function(data){
          if(data['hasil'] == 'success'){
            alert('Save Success');
            $("#searchby_nik_csdm").val($("#nik_csdm").val());
            $("#searchby_name").val($("#name").val());
            table_user.ajax.reload();
            // $('#single_form').trigger("reset");
            // $('#single_form')[0].reset();
            // location.reload();
          }
          else{ 
            alert(data['hasil']); 
            $("#searchby_nik_csdm").val($("#nik_csdm").val());
            table_user.ajax.reload();
          }
        },
        error: function (jXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
      });
    })
  // 
    $("#button_create").click(function(){
      $('#userdata_form')[0].reset();
      // $('#card_form')[0].reset();
      document.getElementById("card_form").style.display = "block";
      $('#card_form').removeClass('card-success');
      $('#card_form').addClass('card-primary');
      $("#card_title").html('CREATE NEW USER FORM');
      $('#button_save').removeClass('btn-success');
      $('#button_save').addClass('btn-primary');
      document.documentElement.scrollTop = 0;
      
    });
  //=======================//


});