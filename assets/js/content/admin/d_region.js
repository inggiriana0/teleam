$(function () {

	//=======================//
	//Upload //
		$('#region_form').submit(function(e){
			e.preventDefault();
			var formdata = new FormData(this);
			formdata.append('sheet', $('#sheet').val());

			var file = $('#file_upload')[0].files[0]
			if (file){
				var fname = file.name;
			 	console.log(fname);
				var re = /(\.xls)$/i;
				if(!re.exec(fname))
				{
					alert("File extension not supported!");
				}else{
					$.ajax({
						url:base_url+'c_admin/do_import_dashboard_region',
						type:"post",
						data:formdata,
						processData:false,
						contentType:false,
						cache:false,
						async:false,
						dataType : 'json',
						success: function(data){
							if (data['hasil'] == 'success') {
								var berhasil_input = data['berhasil_input'];
								alert(berhasil_input+' Upload Data Success');
								$('#tgl_upload').val(data['tgl_upload']);
								$('#uploader').val(data['uploader']);
								table_region.ajax.reload();
							}
							else{
								alert(data['hasil']);
							}
						}
					});
				}
			}		
		});
	//=======================//

	//=======================//
	// var id_project = $('#id_project').val();
	// var nama_project = '';
	// if (id_project == '1') {
	// 	nama_project = 'RED SOCIETY';
	// }else if (id_project == '2'){
	// 	nama_project = 'EDUCATION';
	// }
	// console.log(nama_project);
	//datatable//
	    var table_region = $('#table_region').DataTable({
	      "searching": false,
	      "paging": false,
	      "ordering": false,
	      "lengthChange": false,
	      "autoWidth": false,
	      "responsive": true,
	      "processing": true,
	      "serverSide": true,
      	  "bInfo" : false,
	      // "pageLength": 100,
	      // "order": [[ 1, "asc" ]],
	      dom: 'Bfrtip',
          buttons: ['excel'],
         //  buttons: [
	        //     {
	        //         extend: 'excelHtml5',
	        //         title: nama_project
	        //     }
	        // ],
          // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
	      "columns": [
	        { "data": "region"},
	        { "data": "total" },
	        { "data": "januari" },
	        { "data": "februari" },
	        { "data": "maret" },
	        { "data": "april" },
	        { "data": "mei" },
	        { "data": "juni" },
	        { "data": "juli" },
	        { "data": "agustus" },
	        { "data": "september" },
	        { "data": "oktober" },
	        { "data": "november" },
	        { "data": "desember" }
	        // { 
         //    	"data": "region",
	        //     // render : function(data, type, row, meta) 
	        //     // { return '<div class="col-sm-12"><input type="text" class="form-control form-control-sm text-sm" value="'+row.region+'" id="januari" data-id_data="'+row.id+'"></div>'} 
	        // },
	        // { 
         //    	"data": "total",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.total+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "januari",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.januari+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "februari",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.februari+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "maret",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.maret+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "april",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.april+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "mei",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.mei+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "juni",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.juni+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "juli",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.juli+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "agustus",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.agustus+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "september",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.september+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "oktober",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.oktober+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "november",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.november+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        // { 
         //    	"data": "desember",
	        //     render : function(data, type, row, meta) 
	        //     { return '<input type="text" class="form-control form-control-sm text-xs" value="'+row.desember+'" id="januari" data-id_data="'+row.id+'">'} 
	        // },
	        
	      ],
	      "ajax":{
	        url :base_url+'c_admin/get_dashboard_region', // json datasource
	        type: "post",
	        "dataSrc": function ( json ) {
                //Make your callback here.
                // alert("Done!");
	            // console.log(json);
	            $('#tgl_upload').val(json.data[0]['tgl_upload']);
				$('#uploader').val(json.data[0]['uploader']);
                return json.data;
            },
	        data: function(d){
	          d.searchby_id_project = $('#id_project').val();
	          // d.searchby_name = $('#searchby_name').val();
	        },
	        error: function(){  // error handling code
	          $('#table-element').css("display","none");
	        }
	      }
	   //    "initComplete":function( settings, json){
	   //          // call your function here
	   //          console.log(json);
	   //          console.log(json.data[0]);
	   //          console.log(json.data[0]['tgl_upload']);
	   //          $('#tgl_upload').val(json.data[0]['tgl_upload']);
				// $('#uploader').val(json.data[0]['uploader']);
	   //      }
	    });
	//load data id project onchange//
		$('#id_project').change(function () {
			table_region.ajax.reload();
		});
	//=======================//

});