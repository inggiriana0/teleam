$(function () {

	//=======================//
	//Upload //
		$('#daily_form').submit(function(e){
			e.preventDefault();
			var formdata = new FormData(this);
			formdata.append('sheet', $('#sheet').val());

			var file = $('#file_upload')[0].files[0]
			if (file){
				var fname = file.name;
			 	console.log(fname);
				var re = /(\.xls)$/i;
				if(!re.exec(fname))
				{
					alert("File extension not supported!");
				}else{
					$.ajax({
						url:base_url+'c_admin/do_import_dashboard_daily',
						type:"post",
						data:formdata,
						processData:false,
						contentType:false,
						cache:false,
						async:false,
						dataType : 'json',
						success: function(data){
							if (data['hasil'] == 'success') {
								var berhasil_input = data['berhasil_input'];
								alert(berhasil_input+' Upload Data Success');
								$('#tgl_upload').val(data['tgl_upload']);
								$('#uploader').val(data['uploader']);
								table_daily.ajax.reload();
							}
							else{
								alert(data['hasil']);
							}
						}
					});
				}
			}
			
		});
	//=======================//

	//=======================//
	//datatable//
	    var table_daily = $('#table_daily').DataTable({
	      "searching": false,
	      "paging": false,
	      "ordering": false,
	      "lengthChange": false,
	      "autoWidth": false,
	      "responsive": true,
	      "processing": true,
	      "serverSide": true,
      	  "bInfo" : false,
	      // "pageLength": 100,
	      // "order": [[ 1, "asc" ]],
	      dom: 'Bfrtip',
          buttons: ['excel'],
          // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
	      "columns": [
	        { "data": "tgl" },
	        { "data": "agent" },
	        { "data": "call" },
	        { "data": "contacted" },
	        { "data": "agree" },
	        { "data": "active" },
	        { "data": "revenue" },
	        { "data": "growth_dod" },
	        { "data": "contacted_rate" },
	        { "data": "conversion_rate" },
	        { "data": "active_rate" },
	        { "data": "target" },
	        { "data": "ach" }
	      ],
	      "ajax":{
	        url :base_url+'c_admin/get_dashboard_daily', // json datasource
	        type: "post",
	        data: function(d){
	          // d.searchby_nik_csdm = $('#searchby_nik_csdm').val();
	          // d.searchby_name = $('#searchby_name').val();
	        },
	        error: function(){  // error handling code
	          $('#table-element').css("display","none");
	        }
	      },
	    });
	//=======================//

});