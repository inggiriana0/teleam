$(function () {

  //=======================//
  //create select single_jenis_wl//
    jQuery.ajax({
      url: base_url+'c_single/get_jenis_wl',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_jenis_wl').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['jenis_wl']+'">'+data['data'][i]['jenis_wl']+'</option>';
          $("#single_jenis_wl").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select bulk_jenis_wl//
    jQuery.ajax({
      url: base_url+'c_bulk/get_jenis_wl',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_bulk_jenis_wl').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['jenis_wl']+'">'+data['data'][i]['jenis_wl']+'</option>';
          $("#bulk_jenis_wl").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select takers_jenis_wl//
    jQuery.ajax({
      url: base_url+'c_takers/get_jenis_wl',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_takers_jenis_wl').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['jenis_wl']+'">'+data['data'][i]['jenis_wl']+'</option>';
          $("#takers_jenis_wl").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //get_dashboard_single//
    function get_dashboard_single(){
      jQuery.ajax({
        url: base_url+'dashboard/get_dashboard_single',
        type: 'POST',
        data: {jenis_wl:$('#single_jenis_wl').val()},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
          $("#single_total_call").html('<span class="badge badge-danger">'+data['total_call']+'</span>');
          $("#single_agree").html('<span class="badge badge-success">'+data['agree']+'</span>');
          $("#single_disagree").html('<span class="badge badge-success">'+data['disagree']+'</span>');
          $("#single_follow_up").html('<span class="badge badge-success">'+data['follow_up']+'</span>');
          $("#single_busy").html('<span class="badge badge-success">'+data['busy']+'</span>');
          $("#single_rna").html('<span class="badge badge-success">'+data['rna']+'</span>');
          $("#single_voicemail").html('<span class="badge badge-success">'+data['voicemail']+'</span>');
          $("#single_not_contactable").html('<span class="badge badge-danger">'+data['not_contactable']+'</span>');
          $("#single_contacted_rate").html('<span class="badge badge-danger">'+data['contacted_rate']+' %</span>');
          $("#single_conversion_rate").html('<span class="badge badge-danger">'+data['conversion_rate']+' %</span>');
          $("#single_revenue").html('<span class="badge badge-danger">'+data['revenue']+'</span>');
          $("#single_contacted").html('<span class="badge badge-danger">'+data['contacted']+'</span>');
          $("#single_not_contacted").html('<span class="badge badge-danger">'+data['not_contacted']+'</span>');
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log(textStatus.reponseText);
        }
      });
    }
    jQuery.ajax({
      url: base_url+'dashboard/get_dashboard_single',
      type: 'POST',
      data: {jenis_wl:$('#single_jenis_wl').val()},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $("#single_total_call").html('<span class="badge badge-danger">'+data['total_call']+'</span>');
        $("#single_agree").html('<span class="badge badge-success">'+data['agree']+'</span>');
        $("#single_disagree").html('<span class="badge badge-success">'+data['disagree']+'</span>');
        $("#single_follow_up").html('<span class="badge badge-success">'+data['follow_up']+'</span>');
        $("#single_busy").html('<span class="badge badge-success">'+data['busy']+'</span>');
        $("#single_rna").html('<span class="badge badge-success">'+data['rna']+'</span>');
        $("#single_voicemail").html('<span class="badge badge-success">'+data['voicemail']+'</span>');
        $("#single_not_contactable").html('<span class="badge badge-danger">'+data['not_contactable']+'</span>');
        $("#single_contacted_rate").html('<span class="badge badge-danger">'+data['contacted_rate']+' %</span>');
        $("#single_conversion_rate").html('<span class="badge badge-danger">'+data['conversion_rate']+' %</span>');
        $("#single_revenue").html('<span class="badge badge-danger">'+data['revenue']+'</span>');
        $("#single_contacted").html('<span class="badge badge-danger">'+data['contacted']+'</span>');
        $("#single_not_contacted").html('<span class="badge badge-danger">'+data['not_contacted']+'</span>');
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });

    $("#single_jenis_wl").change(function(){
      get_dashboard_single();
    });
  //=======================//

  //=======================//
  //get_dashboard_bulk//
    // function get_dashboard_bulk(){
      jQuery.ajax({
        url: base_url+'dashboard/get_dashboard_bulk',
        type: 'POST',
        data: {},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
          $("#bulk_total_call").html('<span class="badge badge-danger">'+data['total_call']+'</span>');
          $("#bulk_agree").html('<span class="badge badge-success">'+data['agree']+'</span>');
          $("#bulk_disagree").html('<span class="badge badge-success">'+data['disagree']+'</span>');
          $("#bulk_follow_up").html('<span class="badge badge-success">'+data['follow_up']+'</span>');
          $("#bulk_busy").html('<span class="badge badge-success">'+data['busy']+'</span>');
          $("#bulk_rna").html('<span class="badge badge-success">'+data['rna']+'</span>');
          $("#bulk_voicemail").html('<span class="badge badge-success">'+data['voicemail']+'</span>');
          $("#bulk_not_contactable").html('<span class="badge badge-danger">'+data['not_contactable']+'</span>');
          $("#bulk_contacted_rate").html('<span class="badge badge-danger">'+data['contacted_rate']+' %</span>');
          $("#bulk_conversion_rate").html('<span class="badge badge-danger">'+data['conversion_rate']+' %</span>');
          $("#bulk_revenue").html('<span class="badge badge-danger">'+data['revenue']+'</span>');
          $("#bulk_contacted").html('<span class="badge badge-danger">'+data['contacted']+'</span>');
          $("#bulk_not_contacted").html('<span class="badge badge-danger">'+data['not_contacted']+'</span>');
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log(textStatus.reponseText);
        }
      });
    // }
  //=======================//

  //=======================//
  //get_dashboard_takers//
    function get_dashboard_takers(){
      jQuery.ajax({
        url: base_url+'dashboard/get_dashboard_takers',
        type: 'POST',
        data: {jenis_wl:$('#takers_jenis_wl').val()},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
          $("#takers_total_call").html('<span class="badge badge-danger">'+data['total_call']+'</span>');
          $("#takers_agree").html('<span class="badge badge-success">'+data['agree']+'</span>');
          $("#takers_disagree").html('<span class="badge badge-success">'+data['disagree']+'</span>');
          $("#takers_follow_up").html('<span class="badge badge-success">'+data['follow_up']+'</span>');
          $("#takers_busy").html('<span class="badge badge-success">'+data['busy']+'</span>');
          $("#takers_rna").html('<span class="badge badge-success">'+data['rna']+'</span>');
          $("#takers_voicemail").html('<span class="badge badge-success">'+data['voicemail']+'</span>');
          $("#takers_not_contactable").html('<span class="badge badge-danger">'+data['not_contactable']+'</span>');
          $("#takers_contacted_rate").html('<span class="badge badge-danger">'+data['contacted_rate']+' %</span>');
          $("#takers_conversion_rate").html('<span class="badge badge-danger">'+data['conversion_rate']+' %</span>');
          $("#takers_revenue").html('<span class="badge badge-danger">'+data['revenue']+'</span>');
          $("#takers_contacted").html('<span class="badge badge-danger">'+data['contacted']+'</span>');
          $("#takers_not_contacted").html('<span class="badge badge-danger">'+data['not_contacted']+'</span>');
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log(textStatus.reponseText);
        }
      });
    }
    jQuery.ajax({
      url: base_url+'dashboard/get_dashboard_takers',
      type: 'POST',
      data: {jenis_wl:$('#takers_jenis_wl').val()},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $("#takers_total_call").html('<span class="badge badge-danger">'+data['total_call']+'</span>');
        $("#takers_agree").html('<span class="badge badge-success">'+data['agree']+'</span>');
        $("#takers_disagree").html('<span class="badge badge-success">'+data['disagree']+'</span>');
        $("#takers_follow_up").html('<span class="badge badge-success">'+data['follow_up']+'</span>');
        $("#takers_busy").html('<span class="badge badge-success">'+data['busy']+'</span>');
        $("#takers_rna").html('<span class="badge badge-success">'+data['rna']+'</span>');
        $("#takers_voicemail").html('<span class="badge badge-success">'+data['voicemail']+'</span>');
        $("#takers_not_contactable").html('<span class="badge badge-danger">'+data['not_contactable']+'</span>');
        $("#takers_contacted_rate").html('<span class="badge badge-danger">'+data['contacted_rate']+' %</span>');
        $("#takers_conversion_rate").html('<span class="badge badge-danger">'+data['conversion_rate']+' %</span>');
        $("#takers_revenue").html('<span class="badge badge-danger">'+data['revenue']+'</span>');
        $("#takers_contacted").html('<span class="badge badge-danger">'+data['contacted']+'</span>');
        $("#takers_not_contacted").html('<span class="badge badge-danger">'+data['not_contacted']+'</span>');
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });

    $("#takers_jenis_wl").change(function(){
      takers_jenis_wl = $('#takers_jenis_wl').val();
      if (takers_jenis_wl != 0) {
        get_dashboard_takers();
      }
    });
  //=======================//

  // $('#my-card [data-card-widget="card-refresh"]').on('loaded.lte.cardrefresh', handleLoadedEvent)
  // $("#card-refresh_widget").CardRefresh("load"); 
  
  // $("#card_refresh_widgetbulk").CardRefresh("load"); 


});