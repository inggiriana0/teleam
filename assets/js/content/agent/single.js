$(function () {

  //=======================//
  //Setting datepicker
    $('#tgl_call').datepicker({
      // setDate: new Date(),
      // setDate: Date(),
      autoclose: true
      // container: '#bs_datepicker_range_container'
    });
    $('#tgl_aktif').datepicker({
      autoclose: true
    });
    $('#tgl_cari').datepicker({
      autoclose: true
    });
    // $('#tgl_call').datepicker('update',new Date);

    // $('#bs_datepicker_container input').datepicker({
    //     autoclose: true,
    //     container: '#bs_datepicker_container'
    // });

    // $('#case_date').datetimepicker({
    //   format: 'YYYY-MM-DD HH:mm:ss'
    // });

    // $('#resolved_date').datetimepicker({
    //   format: 'YYYY-MM-DD HH:mm:ss'
    // });
  //=======================//

  //=======================//
  //create select jenis_wl//
    jQuery.ajax({
      url: base_url+'c_single/get_jenis_wl',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_jenis_wl').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['jenis_wl']+'">'+data['data'][i]['jenis_wl']+'</option>';
          $("#jenis_wl").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select jam_call//
    jQuery.ajax({
      url: base_url+'c_single/get_jam_call',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_jam_call').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['jam_call']+'">'+data['data'][i]['jam_call']+'</option>';
          $("#jam_call").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select prt//
    jQuery.ajax({
      url: base_url+'c_single/get_prt',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_prt').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['prt']+'">'+data['data'][i]['prt']+'</option>';
          $("#prt").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select arpu//
    jQuery.ajax({
      url: base_url+'c_single/get_arpu',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        $('.select2_arpu').select2({});
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['arpu']+'">'+data['data'][i]['arpu']+'</option>';
          $("#arpu").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select status_call//
    $('.select2_status_call').select2({});
    jQuery.ajax({
      url: base_url+'c_single/get_status_call',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['id']+'">'+data['data'][i]['status_call']+'</option>';
          $("#status_call").append(o).trigger('change');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
    // $('.select2_status_call').select2({
    //   ajax: {
    //     url: base_url+'c_single/get_status_call2',
    //     dataType: 'json',
    //     data: function (params) {
    //       return { status_call: params.term };
    //     },
    //     processResults: function (data, page) {
    //       return { results: data };
    //     }
    //   }
    // });
  //=======================//

  //=======================//
  //create select reason_call//
    $('.select2_reason_call').select2({});
    // function get_reason_call(){
    //   jQuery.ajax({
    //     url: base_url+'c_single/get_reason_call',
    //     type: 'POST',
    //     data: {id_status_call:$("#status_call").val()},
    //     dataType : 'json',
    //     success: function(data, textStatus, xhr) {
    //       var o = '<option value="" selected disabled>--- REASON CALL ---</option>';
    //       for (var i = 0; i < Object.keys(data['data']).length; i++) {
    //         o += '<option value="'+data['data'][i]['id']+'" >'+data['data'][i]['reason_call']+'</option>';
    //       }
    //       $("#reason_call").html(o).trigger('change');
    //     },
    //     error: function(xhr, textStatus, errorThrown) {
    //       console.log(textStatus.reponseText);
    //     }
    //   });
    // }
    function get_reason_call2(reason_call = '', subreason_call = ''){
      if(reason_call !=''){
        jQuery.ajax({
          url: base_url+'c_single/get_reason_call',
          type: 'POST',
          data: {id_status_call:$("#status_call").val()},
          dataType : 'json',
          success: function(data, textStatus, xhr) {
            console.log("ifreason="+reason_call);
            var o = '<option value="" selected disabled>--- REASON CALL ---</option>';
            for (var i = 0; i < Object.keys(data['data']).length; i++) {
              var a = '';
              if(reason_call == data['data'][i]['id']){ a = 'selected';}
              o += '<option value="'+data['data'][i]['id']+'" '+a+'>'+data['data'][i]['reason_call']+'</option>';
            }
            $("#reason_call").html(o).trigger('change');
            if(subreason_call !=''){
              get_subreason_call2(subreason_call);
            }
          },
          error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
          }
        });
      }
      else{
        jQuery.ajax({
          url: base_url+'c_single/get_reason_call',
          type: 'POST',
          data: {id_status_call:$("#status_call").val()},
          dataType : 'json',
          success: function(data, textStatus, xhr) {
            console.log("elsereason="+reason_call);
            var o = '<option value="" selected disabled>--- REASON CALL ---</option>';
            for (var i = 0; i < Object.keys(data['data']).length; i++) {
              o += '<option value="'+data['data'][i]['id']+'" >'+data['data'][i]['reason_call']+'</option>';
            }
            $("#reason_call").html(o).trigger('change');
          },
          error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
          }
        });
      }

    }
    $("#status_call").change(function(){
      get_reason_call2();
    });
  //=======================//

  //=======================//
  //create select subreason_call//
    $('.select2_subreason_call').select2({});
    // function get_subreason_call(){
    //   jQuery.ajax({
    //     url: base_url+'c_single/get_subreason_call',
    //     type: 'POST',
    //     data: {id_reason_call:$("#reason_call").val()},
    //     dataType : 'json',
    //     success: function(data, textStatus, xhr) {
    //       var o = '<option value="" selected disabled>--- SUBREASON CALL ---</option>';
    //       for (var i = 0; i < Object.keys(data['data']).length; i++) {
    //         var a = '';
    //         if(subreason_call == data['data'][i]['id']){ a = 'selected'; }
    //         o += '<option value="'+data['data'][i]['id']+'" data-ket="'+data['data'][i]['ket']+'"'+a+'>'+data['data'][i]['subreason_call']+'</option>';
    //       }
    //       $("#subreason_call").html(o).trigger('change');
    //       // console.log("tes");
    //     },
    //     error: function(xhr, textStatus, errorThrown) {
    //       console.log(textStatus.reponseText);
    //     }
    //   });
    // }
    function get_subreason_call2(subreason_call = ''){
      if(subreason_call !=''){
        jQuery.ajax({
          url: base_url+'c_single/get_subreason_call',
          type: 'POST',
          data: {id_reason_call:$("#reason_call").val()},
          dataType : 'json',
          success: function(data, textStatus, xhr) {
            console.log("ifsubreason_call="+subreason_call);
            var o = '<option value="" selected disabled>--- SUBREASON CALL ---</option>';
            for (var i = 0; i < Object.keys(data['data']).length; i++) {
              var a = '';
              if(subreason_call == data['data'][i]['id']){ a = 'selected'; }
              o += '<option value="'+data['data'][i]['id']+'" data-ket="'+data['data'][i]['ket']+'"'+a+'>'+data['data'][i]['subreason_call']+'</option>';
            }
            $("#subreason_call").html(o).trigger('change');
          },
          error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
          }
        });   
      }
      else{
        jQuery.ajax({
          url: base_url+'c_single/get_subreason_call',
          type: 'POST',
          data: {id_reason_call:$("#reason_call").val()},
          dataType : 'json',
          success: function(data, textStatus, xhr) {
            console.log("elsesubreason_call="+subreason_call);
            var o = '<option value="" selected disabled>--- SUBREASON CALL ---</option>';
            for (var i = 0; i < Object.keys(data['data']).length; i++) {
              var a = '';
              o += '<option value="'+data['data'][i]['id']+'" data-ket="'+data['data'][i]['ket']+'">'+data['data'][i]['subreason_call']+'</option>';
            }
            $("#subreason_call").html(o).trigger('change');
          },
          error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
          }
        }); 
      }

    }
    $("#reason_call").change(function(){
      get_subreason_call2();
      var reason_call = $('#reason_call option:selected').val();
      // $("#paket_sebelumnya").val(status_call);
      if(reason_call == '1') { 
        document.getElementById("paket_sebelumnya").readOnly = false;
        document.getElementById("revenue").readOnly = false;
        document.getElementById("delta_revenue").readOnly = false;
        document.getElementById("paket_aktif").disabled = false;
        document.getElementById("tgl_aktif").disabled = false;
      }else{
        document.getElementById("paket_sebelumnya").readOnly = true;
        document.getElementById("revenue").readOnly = true;
        document.getElementById("delta_revenue").readOnly = true;
        document.getElementById("paket_aktif").disabled = true;
        document.getElementById("tgl_aktif").disabled = true;
        // $("#paket_aktif").val("0").trigger('change');
        // $("#tgl_aktif").val("");
      }
    });
    $("#subreason_call").change(function(){
      var ket = $('#subreason_call option:selected').data('ket');
      console.log("ket"+ket);
      if(typeof ket == 'undefined' || ket =='' || ket == null) { 
        $("#ket_subreason_call").html(''); 
      }else{
        $("#ket_subreason_call").html('*'+ket);
      }
    });
  //=======================//

  //=======================//
  //create select paket_aktif//
    $('.select2_paket_aktif').select2({});
    jQuery.ajax({
      url: base_url+'c_single/get_paket_aktif',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        var o = '<option value="0">--- PAKET AKTIF ---</option>';
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          o += '<option value="'+data['data'][i]['paket_aktif']+'">'+data['data'][i]['paket_aktif']+'</option>';
        }
        $("#paket_aktif").append(o).trigger('refresh');
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select wl_source//
    $('.select2_wl_source').select2({});
    jQuery.ajax({
      url: base_url+'c_single/get_wl_source',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['wl_source']+'">'+data['data'][i]['wl_source']+'</option>';
          $("#wl_source").append(o).trigger('refresh');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
  //=======================//

  //=======================//
  //create select region//
    $('.select2_region').select2({});
    jQuery.ajax({
      url: base_url+'c_single/get_region',
      type: 'POST',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, xhr) {
        for (var i = 0; i < Object.keys(data['data']).length; i++) {
          var o = '<option value="'+data['data'][i]['region']+'" data-area="'+data['data'][i]['area']+'">'+data['data'][i]['region']+'</option>';
          $("#region").append(o).trigger('refresh');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        console.log(textStatus.reponseText);
      }
    });
    $("#region").change(function(){
      var area = $('#region option:selected').data('area');
      console.log("area"+area);
      if(typeof area == 'undefined' || area =='' || area == null) { 
        $("#area").html(''); 
      }else{
        $("#area").val(area);
      }
    });
  //=======================//

  $('#example1').DataTable({
    // "paging": true,
    // "lengthChange": false,
    // "searching": true,
    // "ordering": true,
    "autoWidth": false,
    "responsive": true,
    "pageLength": 50,
    "order": [[ 0, "desc" ]]
  });

  //=======================//
  //single datatable//
    var table_single = $('#table_single').DataTable({
      "searching": false,
      // "paging": true,
      // "ordering": true,
      // "lengthChange": false,
      "autoWidth": false,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "pageLength": 10,
      "order": [[ 0, "desc" ]],
      "columns": [
        { "data": "created" },
        { "data": "msisdn" },
        { "data": "jenis_wl" },
        { "data": "tgl_call" },
        { "data": "tgl_aktif" },
        { "data": "name_status_call" },
        { 
            "data": "id",
            render : function(data, type, row, meta) 
            { return "<button type='button' data-id_single='"+data+"' class='btn btn-info btn-sm'><i class='fas fa-pencil-alt'></i> Edit</button>"} 
        },
      ],
      "ajax":{
        url :base_url+'c_single/get_single', // json datasource
        type: "post",
        data: function(d){
          d.tgl_cari = $('#tgl_cari').val();
          // d.date_end = $('#tgl_cari').val();
          // d.csdm = $('#csdm').val();
          // d.msisdn = $('#msisdn5').val();
        },
        error: function(){  // error handling code
          $('#table-element').css("display","none");
        }
      }
    });
  //button_filter single
    $('#button_filter').click(function(){
      table_single.ajax.reload();
    });
  //single edit button
    $('#table_single tbody').on('click', 'button', function(){
      var id_single = $(this).data('id_single');
      // var kolom = this.className;
      jQuery.ajax({
        url: base_url+'c_single/get_single_byid',
        type: 'POST',
        data: {id_single:id_single},
        dataType : 'json',
        success: function(data, textStatus, xhr) {
          // console.log(data);
          document.getElementById("date_create_edit").style.display = "block";
          $('#card_form').removeClass('card-primary');
          $('#card_form').addClass('card-green');
          $("#card_title").html('EDIT FORM');
          $('#button_save').removeClass('btn-primary');
          $('#button_save').addClass('btn-success');

          $("#id_single").val(data['id']);
          $("#jenis_wl").val(data['jenis_wl']).trigger('change');
          $("#tgl_call").val(data['tgl_call']);
          $("#jam_call").val(data['jam_call']).trigger('change');
          $("#tgl_aktif").val(data['tgl_aktif']);
          $("#msisdn").val(data['msisdn']);
          $("#nama_pelanggan_dsc").val(data['nama_pelanggan_dsc']);
          $("#nama_akun").val(data['nama_akun']);
          $("#prt").val(data['prt']).trigger('change');
          $("#los").val(data['los']);
          $("#arpu").val(data['arpu']).trigger('change');
          $("#status_call").val(data['status_call']).trigger('change');
          // get_reason_call(data['reason_call']);
          get_reason_call2(data['reason_call'], data['subreason_call']);
          // get_subreason_call(data['subreason_call']);
          // $("#subreason_call").val(data['subreason_call']).trigger('change');
          $("#paket_sebelumnya").val(data['paket_sebelumnya']);
          $("#paket_aktif").val(data['paket_aktif']).trigger('change');
          $("#revenue").val(data['revenue']);
          $("#delta_revenue").val(data['delta_revenue']);
          $("#wl_source").val(data['wl_source']).trigger('change');
          $("#region").val(data['region']).trigger('change');
          $("#area").val(data['area']);
          $("#keterangan").val(data['keterangan']);  
          $("#created").val(data['created']);  
          $("#last_edited").val(data['last_edited']); 

          //scroll to the top of the document
          document.documentElement.scrollTop = 0;
        },
        error: function(xhr, textStatus, errorThrown) {
          console.log(textStatus.reponseText);
        }
      });
    })
  //=======================//

  //=======================//
  //single input form//
    $('#single_form').submit(function(e){
      e.preventDefault();
      $.ajax({
        url : base_url + 'c_single/do_insert',
        type : 'POST',
        data : $(this).serialize(),
          dataType : 'json',
        success : function(data){
          if(data['hasil'] == 'success'){
            alert('Berhasil');
            // table_single.ajax.reload();
            // $('#single_form').trigger("reset");
            // $('#single_form')[0].reset();
            location.reload();
          }
          else{ alert(data['hasil']); }
        },
        error: function (jXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
      });
    })
  //reset single form
    $('#reset_form').click(function(){
      // $('#single_form')[0].reset();
      location.reload();
    });
  //=======================//

});