<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (isset($this->session->logged_in) && $this->session->logged_in) {
			if ($this->session->user_level == "ADMIN") {
				redirect($this->session->user_level.'/dashboard');
			}else{
				redirect($this->session->user_level.'/home');
			}
		}
		// if(isset($this->session->logged_in) && $this->session->logged_in) redirect($this->session->user_level.'/home');
	}

	public function index()
	{
		$ip = $this->input->ip_address();

		$explodeip = explode('.',$ip);
		
		$a = $explodeip[0];
		$b = $explodeip[1];
		$c = $explodeip[2];
		$d = $explodeip[3];

		$rangeip = $a.".".$b.".".$c;
		
		// print_r($rangeip);
		if (($rangeip == "172.28.154") || ($rangeip == "172.28.130") || ($rangeip == "172.24.51") ) {
			$this->load->view('login');
		} else {
			$data = array(
				'ip' 		=> $ip,
				'rangeip' 	=> $rangeip
			);
			$this->load->view('error', $data);
		}

	}

}
