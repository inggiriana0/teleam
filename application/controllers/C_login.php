<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_user');
	}

	public function do_sign_in()
	{
		// echo "string";die;
		$data = array();
		$query = $this->m_user->get_user($this->input->post('username'), $this->input->post('password'));
		$row = $query->row();
		// print_r($row);die;
		if(isset($row))
		{
			if ($row->user_level=='SPV' || $row->user_level=='TL')
			{
				$newdata = array(
				        'id'  			=> $row->id,
				        'nik_csdm' 		=> $row->nik_csdm,
				        'name'     		=> $row->name,
				        'leader'		=> $row->leader,
				        'jabatan'		=> $row->jabatan,
				        'site'			=> $row->site,
				        'gender'		=> $row->gender,
				        'user_level'	=> $row->user_level,
				        'logged_in' 	=> TRUE
				);
				$this->session->set_userdata($newdata);
				$this->m_user->do_update_is_login_user($this->session->id);
				$data['hasil'] = 'success';
				$data['url'] = 'tl_spv/home';
			}
			else if ($row->user_level=='ADMIN')
			{
				$newdata = array(
				        'id'  			=> $row->id,
				        'nik_csdm' 		=> $row->nik_csdm,
				        'name'     		=> $row->name,
				        'leader'		=> $row->leader,
				        'jabatan'		=> $row->jabatan,
				        'site'			=> $row->site,
				        'gender'		=> $row->gender,
				        'user_level'	=> $row->user_level,
				        'logged_in' 	=> TRUE
				);
				$this->session->set_userdata($newdata);
				$this->m_user->do_update_is_login_user($this->session->id);
				$data['hasil'] = 'success';
				$data['url'] = 'admin/dashboard';
				// print_r($row);die;
			}
			else
			{
				$newdata = array(
				        'id'  			=> $row->id,
				        'nik_csdm' 		=> $row->nik_csdm,
				        'name'     		=> $row->name,
				        'leader'		=> $row->leader,
				        'jabatan'		=> $row->jabatan,
				        'site'			=> $row->site,
				        'gender'		=> $row->gender,
				        'user_level'	=> $row->user_level,
				        'logged_in' 	=> TRUE
				);
				$this->session->set_userdata($newdata);
				$this->m_user->do_update_is_login_user($this->session->id);
				$data['hasil'] = 'success';
				$data['url'] = 'agent/home';
				// print_r($row);die;
			}
		}
		else
		{
			$data['hasil'] = 'Username dan Password Anda Belum Terdaftar';
		}
		echo json_encode($data);
	}

	public function do_log_out(){
		$this->m_user->do_update_is_logout_user($this->session->id);
		session_destroy();
		redirect('login');
	}

}
