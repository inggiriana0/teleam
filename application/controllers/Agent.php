<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

	function __construct() {
        parent::__construct();
		// if ($this->session->logged_in != TRUE) {
		// 	redirect('login');
		// }
		if(!$this->session->logged_in) redirect('login');
    }

	// public function home()
	// {
	// 	$this->load->view('master');
	// }

	public function home()
	{
		$this->load->model('m_single');
		$row = $this->m_single->get_total_home_useragent()->row_array();
		$data = array(
			'data'			=> $row,
			'page' 			=> 'home',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> $this->session->user_level
		);
		$this->load->view('master', $data);
	}

	public function single()
	{
		$data = array(
			'page' 			=> 'single',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> $this->session->user_level
		);
		$this->load->view('master', $data);
	}

	public function bulk()
	{
		$data = array(
			'page'			=> 'bulk',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> $this->session->user_level
		);
		$this->load->view('master', $data);
	}

	public function takers()
	{
		$data = array(
			'page'			=> 'takers',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> $this->session->user_level
		);
		$this->load->view('master', $data);
	}
}
