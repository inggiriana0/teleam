<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_takers extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_takers');
	}

	public function do_insert(){
		// echo date('Y-m-d');die;
		$data = array(
			'nik_csdm'			=> $this->input->post('nik_csdm'),
			'name'				=> $this->input->post('name'),
			'leader'			=> $this->input->post('leader'),
			'site'				=> $this->input->post('site'),
			'jenis_wl'			=> $this->input->post('jenis_wl'),
			'tgl_call'			=> $this->input->post('tgl_call'),
			'jam_call'			=> $this->input->post('jam_call'),
			'tgl_aktif'			=> $this->input->post('tgl_aktif'),
			'msisdn'			=> $this->input->post('msisdn'),
			'nama_pelanggan_dsc'=> $this->input->post('nama_pelanggan_dsc'),
			'nama_akun'			=> $this->input->post('nama_akun'),
			'prt'				=> $this->input->post('prt'),
			'los'				=> $this->input->post('los'),
			'arpu'				=> $this->input->post('arpu'),
			'status_call'		=> $this->input->post('status_call'),
			'reason_call'		=> $this->input->post('reason_call'),
			'subreason_call'	=> $this->input->post('subreason_call'),
			'paket_sebelumnya'	=> $this->input->post('paket_sebelumnya'),
			'paket_penawaran'	=> $this->input->post('paket_penawaran'),
			'revenue'			=> $this->input->post('revenue'),
			'estimasi_revenue'	=> $this->input->post('estimasi_revenue'),
			'delta_revenue'		=> $this->input->post('delta_revenue'),
			'wl_source'			=> $this->input->post('wl_source'),
			'region'			=> $this->input->post('region'),
			'area'				=> $this->input->post('area'),
			'project'			=> $this->input->post('project'),
			'keterangan'		=> $this->input->post('keterangan')
		);
		// print_r($data);die;
		$id_takers = $this->input->post('id_takers');

		if($id_takers == '0'){

			$query = $this->m_takers->do_insert($data);
			// echo $query['success'];die;
			if ($query['success']){
				echo json_encode(array('hasil'=>'success'));
			}else{
				echo json_encode(array('hasil'=>$query['error']['message']));
			}
		}
		else{

			$query = $this->m_takers->do_update($data,$id_takers);
			// echo $query['success'];die;
			if ($query['success'] == 1){
				echo json_encode(array('hasil'=>'success'));
			}else{
				echo json_encode(array('hasil'=>$query['error']['message']));
			}
		}	
	}

	public function get_takers()
	{
		$params = $_REQUEST;
		$tgl_cari = $params['tgl_cari'];
		// $csdm = $params['csdm'];
		// $msisdn = $params['msisdn'];
		// $csdm_created = $params['csdm_created'];

		$a = '';
		// if($csdm!=''){
		// 	$a.=" AND a.csdm_agent = '".$csdm."'";
		// }

		// if($msisdn!=''){
		// 	$a.=" AND a.msisdn LIKE '%".$msisdn."%'";
		// }

		// if($csdm_created!=''){
		// 	$a.=" AND a.csdm_requester = '".$csdm_created."'";
		// }

		if($this->session->user_level == 'AGENT'){
			if($tgl_cari!='') $a.=" AND DATE_FORMAT(a.created, '%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($tgl_cari))."' AND '".date('Y-m-d',strtotime($tgl_cari))."' ";
			$a.=" AND a.nik_csdm = '".$this->session->nik_csdm."'";
		}else{
			list($startDate, $endDate) = explode(' - ', $tgl_cari);		
			if($tgl_cari!='') $a.=" AND DATE_FORMAT(a.created, '%Y-%m-%d') BETWEEN '".date('Y-m-d',strtotime($startDate))."' AND '".date('Y-m-d',strtotime($endDate))."' ";
		}
		
		$columns = array(
			0 =>'a.created',
			1 =>'a.msisdn', 
			2 => 'a.jenis_wl',
			3 => 'a.tgl_call',
			4 => 'a.tgl_aktif',
			5 => 'name_status_call'
		);
		$query=$this->m_takers->get_takers_severside(
			$columns[$params['order'][0]['column']], 
			$params['order'][0]['dir'], 
			$params['start'], 
			$params['length'],$a);
		$total = $this->m_takers->get_total_takers_severside($a)->row();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => $total->total,  
			"recordsFiltered" => $total->total,
			"data"            => $data['data']   // total data array
			);
		echo json_encode($json_data);
	}

	public function get_takers_byid(){
		$id_takers = $this->input->post('id_takers');
		$query = $this->m_takers->get_takers_byid($id_takers)->row_array();
		$data = $query;
		echo json_encode($data);
	}

	public function get_xls($month, $day, $year, $month2, $day2, $year2)
	{
		$this->load->library("excel");
		$this->excel->load(APPPATH."../uploads/export_takers.xlsx");
		$this->excel->setActiveSheetIndex(0);

		$a = '';

		$a.=" AND DATE_FORMAT(a.created, '%Y-%m-%d') BETWEEN '".$year."-".$month."-".$day."' AND '".$year2."-".$month2."-".$day2."'";

		// if($agent!='-'){
		// 	$a.=" AND b.csdm LIKE '%".$agent."%'";
		// }
		// if($filter_level1!='-'){
		// 	$a.=" AND e.id = '".$filter_level1."'";
		// }

		$star = strtotime($month.'/'.$day.'/'.$year);
		$end = strtotime($month2.'/'.$day2.'/'.$year2);

		$this->excel->getActiveSheet()->SetCellValue('A2', date('Y-m-d', $star).'  to  '. date('Y-m-d', $end));

		$query=$this->m_takers->get_takers_export($a);
		
		$i=1;
		$row=5;
		foreach ($query->result_array() as $myRow) {
			$this->excel->getActiveSheet()->SetCellValue('A'.$row, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$row, $myRow['nik_csdm']);
			$this->excel->getActiveSheet()->SetCellValue('C'.$row, $myRow['name']);
			$this->excel->getActiveSheet()->SetCellValue('D'.$row, $myRow['leader']);
			$this->excel->getActiveSheet()->SetCellValue('E'.$row, $myRow['site']);
			$this->excel->getActiveSheet()->SetCellValue('F'.$row, $myRow['jenis_wl']);
			$this->excel->getActiveSheet()->SetCellValue('G'.$row, $myRow['tgl_call']);
			$this->excel->getActiveSheet()->SetCellValue('H'.$row, $myRow['jam_call']);
			$this->excel->getActiveSheet()->SetCellValue('I'.$row, $myRow['tgl_aktif']);
			$this->excel->getActiveSheet()->SetCellValue('J'.$row, $myRow['msisdn']);
			$this->excel->getActiveSheet()->SetCellValue('K'.$row, $myRow['nama_pelanggan_dsc']);
			$this->excel->getActiveSheet()->SetCellValue('L'.$row, $myRow['nama_akun']);
			$this->excel->getActiveSheet()->SetCellValue('M'.$row, $myRow['prt']);
			$this->excel->getActiveSheet()->SetCellValue('N'.$row, $myRow['los']);
			$this->excel->getActiveSheet()->SetCellValue('O'.$row, $myRow['arpu']);
			$this->excel->getActiveSheet()->SetCellValue('P'.$row, $myRow['name_status_call']);
			$this->excel->getActiveSheet()->SetCellValue('Q'.$row, $myRow['name_reason_call']);
			$this->excel->getActiveSheet()->SetCellValue('R'.$row, $myRow['name_subreason_call']);
			$this->excel->getActiveSheet()->SetCellValue('S'.$row, $myRow['paket_sebelumnya']);
			$this->excel->getActiveSheet()->SetCellValue('T'.$row, $myRow['paket_penawaran']);
			$this->excel->getActiveSheet()->setCellValueExplicit('U'.$row, $myRow['revenue']);
			$this->excel->getActiveSheet()->setCellValueExplicit('V'.$row, $myRow['estimasi_revenue']);
			$this->excel->getActiveSheet()->setCellValueExplicit('W'.$row, $myRow['delta_revenue']);
			$this->excel->getActiveSheet()->SetCellValue('X'.$row, $myRow['wl_source']);
			$this->excel->getActiveSheet()->SetCellValue('Y'.$row, $myRow['region']);
			$this->excel->getActiveSheet()->SetCellValue('Z'.$row, $myRow['area']);
			$this->excel->getActiveSheet()->SetCellValue('AA'.$row, $myRow['project']);
			$this->excel->getActiveSheet()->SetCellValue('AB'.$row, $myRow['keterangan']);
			$this->excel->getActiveSheet()->SetCellValue('AC'.$row, $myRow['created']);
			$this->excel->getActiveSheet()->SetCellValue('AD'.$row, $myRow['last_edited']);
			$i++;
			$row++;
		}

		$this->excel->getActiveSheet()->getStyle('A5:AD'.($row-1))->applyFromArray(
			array(
			'borders' => array(
					'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                // 'color' => array('rgb' => 'DDDDDD')
		            )
				)
 			)
		);
		
		// die;
		$this->excel->stream("TAKERS.xlsx");
	}

	public function get_jenis_wl()
	{
		$query=$this->m_takers->get_jenis_wl();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_jam_call()
	{
		$query=$this->m_takers->get_jam_call();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_prt()
	{
		$query=$this->m_takers->get_prt();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_arpu()
	{
		$query=$this->m_takers->get_arpu();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_status_call()
	{
		$query=$this->m_takers->get_status_call();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_reason_call()
	{
		$id_status_call = $this->input->post('id_status_call');
		$query=$this->m_takers->get_reason_call($id_status_call);
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_subreason_call()
	{
		$id_reason_call = $this->input->post('id_reason_call');
		$query=$this->m_takers->get_subreason_call($id_reason_call);
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_paket_penawaran()
	{
		$query=$this->m_takers->get_paket_penawaran();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_wl_source()
	{
		$query=$this->m_takers->get_wl_source();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_region()
	{
		$query=$this->m_takers->get_region();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

	public function get_project()
	{
		$query=$this->m_takers->get_project();
		$data['data']= array();
		$i=0;

		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}

		echo json_encode($data);
	}

}
