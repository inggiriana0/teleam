<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
        parent::__construct();
		// if ($this->session->logged_in != TRUE) {
		// 	redirect('login');
		// }
		if(!$this->session->logged_in) redirect('login');
		$this->load->model('m_dashboard');

    }

	// public function home()
	// {
	// 	$this->load->view('master');
	// }

	public function dashboard()
	{
		// $this->load->library('../controllers/whathever');
		$row = $this->m_dashboard->get_total_call()->row_array();
		$data = array(
			'data'			=> $row,
			'page' 			=> 'dashboard',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> $this->session->user_level
		);
		$this->load->view('master', $data);
	}

	public function user()
	{
		$data = array(
			'page' 			=> 'user',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> $this->session->user_level
		);
		$this->load->view('master', $data);
	}

	public function single()
	{
		$data = array(
			'page' 			=> 'single',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function bulk()
	{
		$data = array(
			'page'			=> 'bulk',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function takers()
	{
		$data = array(
			'page'			=> 'takers',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function d_summary()
	{
		$row = $this->m_dashboard->get_dashboard_summary()->row_array();
		$data = array(
			'data'			=> $row,
			'page'			=> 'd_summary',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function d_daily()
	{
		$this->load->model('m_admin');
		$row = $this->m_admin->get_flag_dashboard_daily()->row_array();
		$data = array(
			'data'			=> $row,
			'page'			=> 'd_daily',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function d_monthly()
	{
		$this->load->model('m_admin');
		$row = $this->m_admin->get_flag_dashboard_monthly()->row_array();
		$data = array(
			'data'			=> $row,
			'page'			=> 'd_monthly',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function d_project()
	{
		$data = array(
			'page'			=> 'd_project',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function d_region()
	{
		$data = array(
			'page'			=> 'd_region',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

	public function d_bestperformance()
	{
		$data = array(
			'page'			=> 'd_bestperformance',
			'sidebar' 		=> 'sidebar_admin',
			'user_level' 	=> 'admin'
		);
		$this->load->view('master', $data);
	}

}
