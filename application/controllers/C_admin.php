<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_admin');
		$this->load->model('m_dashboard');
	}

	public function do_insert(){
		$data = array(
			'nik_csdm'			=> $this->input->post('nik_csdm'),
			'name'				=> $this->input->post('name'),
			'leader'			=> $this->input->post('leader'),
			'jabatan'			=> $this->input->post('jabatan'),
			'site'				=> $this->input->post('site'),
			'gender'			=> $this->input->post('gender'),
			'user_level'		=> $this->input->post('user_level'),
			'status_user'		=> $this->input->post('status_user')
		);
		// print_r($data);die;
		$id_tb_user = $this->input->post('id_tb_user');

		if($id_tb_user == '0'){
			// print_r($data);die;
			$query = $this->m_admin->do_insert($data);
			// echo $query['success'];die;
			if ($query['success']){
				echo json_encode(array('hasil'=>'success'));
			}else{
				echo json_encode(array('hasil'=>$query['error']['message']));
			}
		}
		else{

			$query = $this->m_admin->do_update($data,$id_tb_user);
			// echo $query['success'];die;
			if ($query['success'] == 1){
				echo json_encode(array('hasil'=>'success'));
			}else{
				echo json_encode(array('hasil'=>$query['error']['message']));
			}
		}
	}

	public function get_tb_user(){
		$params = $_REQUEST;
		$nik_csdm = $params['searchby_nik_csdm'];
		$name = $params['searchby_name'];

		$a = '';
		if($nik_csdm!=''){
			$a.=" AND a.nik_csdm = '".$nik_csdm."'";
		}

		if($name!=''){
			$a.=" AND a.name LIKE '%".$name."%'";
		}
		
		$columns = array(
			0 =>'a.nik_csdm',
			1 =>'a.name', 
			2 => 'a.site',
			3 => 'a.user_level',
			4 => 'a.last_login'
		);
		$query=$this->m_admin->get_tb_user_severside(
			$columns[$params['order'][0]['column']], 
			$params['order'][0]['dir'], 
			$params['start'], 
			$params['length'],$a);
		$total = $this->m_admin->get_total_tb_user_severside($a)->row();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => $total->total,  
			"recordsFiltered" => $total->total,
			"data"            => $data['data']   // total data array
			);
		echo json_encode($json_data);
	}

	public function get_tb_user_byid(){
		$id_tb_user = $this->input->post('id_tb_user');
		$query = $this->m_admin->get_tb_user_byid($id_tb_user)->row_array();
		$data = $query;
		echo json_encode($data);
	}

	public function get_xls($searchby_nik_csdm, $searchby_name){
		$this->load->library("excel");
		$this->excel->load(APPPATH."../uploads/export_user.xlsx");
		$this->excel->setActiveSheetIndex(0);

		$a = '';

		if($searchby_nik_csdm!='-'){
			$a.=" AND a.nik_csdm = '".$searchby_nik_csdm."'";
		}
		if($searchby_name!='-'){
			$a.=" AND a.name LIKE '%".$searchby_name."%'";
		}

		$query=$this->m_admin->get_tb_user_export($a);
		
		$i=1;
		$row=5;
		foreach ($query->result_array() as $myRow) {
			$this->excel->getActiveSheet()->SetCellValue('A'.$row, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$row, $myRow['nik_csdm']);
			$this->excel->getActiveSheet()->SetCellValue('C'.$row, $myRow['name']);
			$this->excel->getActiveSheet()->SetCellValue('D'.$row, $myRow['leader']);
			$this->excel->getActiveSheet()->SetCellValue('E'.$row, $myRow['jabatan']);
			$this->excel->getActiveSheet()->SetCellValue('F'.$row, $myRow['site']);
			$this->excel->getActiveSheet()->SetCellValue('G'.$row, $myRow['gender']);
			$this->excel->getActiveSheet()->SetCellValue('H'.$row, $myRow['user_level']);
			$this->excel->getActiveSheet()->SetCellValue('I'.$row, $myRow['last_login']);
			$i++;
			$row++;
		}

		$this->excel->getActiveSheet()->getStyle('A5:I'.($row-1))->applyFromArray(
			array(
			'borders' => array(
					'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                // 'color' => array('rgb' => 'DDDDDD')
		            )
				)
 			)
		);
		
		// die;
		$this->excel->stream("USER DATA.xlsx");
	}

	public function get_tes(){
		// $id_tb_user = $this->input->post('id_tb_user');
		$row = $this->m_dashboard->get_dashboard_single()->row_array();
		echo $row['total_call'];

		// $query = $this->m_admin->get_tb_user_byid($id_tb_user)->row_array();
		// $data = $query;
		// echo json_encode($data);
	}

	public function do_update_dashboard_summary(){
		$data = array(
			'call'			=> $this->input->post('call'),
			'contacted'		=> $this->input->post('contacted'),
			'agree'			=> $this->input->post('agree'),
			'aktif'			=> $this->input->post('aktif'),
			'target_eoy'	=> $this->input->post('target_eoy'),
			'revenue'		=> $this->input->post('revenue'),
			'gap_rev_eoy'	=> $this->input->post('gap_rev_eoy'),
			'ach_eoy'		=> $this->input->post('ach_eoy'),
			'west_revenue'	=> $this->input->post('west_revenue'),
			'east_revenue'	=> $this->input->post('east_revenue'),
			'edited_by'		=> $this->session->name
		);
		// print_r($data);die;

		$query = $this->m_admin->do_update_dashboard_summary($data, 1);
		// echo $query['success'];die;
		if ($query['success'] == 1){
			echo json_encode(array('hasil'=>'success'));
		}else{
			echo json_encode(array('hasil'=>$query['error']['message']));
		}
		
	}

	public function do_import_dashboard_daily(){
		$this->load->library('Spreadsheet_Excel_Reader');

		$today = date("Y-m-d");
		
		if(isset($_FILES)){

			$getflag = $this->m_admin->get_flag_dashboard_daily()->row_array();
			$flagbefore = $getflag['flag'];
			$flagafter = $getflag['id'];

			if ($flagafter == null) {
				$flagafter = 1;
			}else{
				$flagafter;
			}
			// echo $getflag['flag'];die;

			$data = new Spreadsheet_Excel_Reader($_FILES['file_upload']['tmp_name']);
			$row = 0;
			$col = 0;
			// print_r($_POST); die;
			$berhasil_input = 0;
			for($sheet = 0; $sheet < intval($this->input->post('sheet')); $sheet++)
			{
				$jumlahbaris = $data->rowcount($sheet_index=$sheet);
				$row = 4;
				while ($row <= $jumlahbaris && $data->val($row,1,$sheet)!='') 
				{
					$result = $this->m_admin->do_insert_dashboard_daily($data->val($row,1,$sheet), $data->val($row,2,$sheet), $data->val($row,3,$sheet), $data->val($row,4,$sheet), $data->val($row,5,$sheet), $data->val($row,6,$sheet), $data->val($row,7,$sheet), $data->val($row,8,$sheet), $data->val($row,9,$sheet), $data->val($row,10,$sheet), $data->val($row,11,$sheet), $data->val($row,12,$sheet), $data->val($row,13,$sheet), $this->session->name, $flagafter);
					if($result != 1) {
						echo json_encode(array('hasil'=>'gagal upload'));
					}

					if ($result == 1) {
						$berhasil_input++;
					}
					$row ++;
				}
			}
			$total = 'TOTAL';
			$result_total = $this->m_admin->do_insert_dashboard_daily($total, $data->val(2,2), $data->val(2,3), $data->val(2,4), $data->val(2,5), $data->val(2,6), $data->val(2,7), $data->val(2,8), $data->val(2,9), $data->val(2,10), $data->val(2,11), $data->val(2,12), $data->val(2,13), $this->session->name, $flagafter);

			if ($result_total == 1) {
				$this->m_admin->do_clear_data_dashboard_daily($flagafter);
				$getlastupload = $this->m_admin->get_flag_dashboard_daily()->row_array();
				$tgl_upload = $getlastupload['tgl_upload'];
				$uploader = $getlastupload['uploader'];
				echo json_encode(array('hasil'=>'success', 'berhasil_input'=>$berhasil_input, 'tgl_upload'=>$tgl_upload, 'uploader'=>$uploader));
			}else{
				echo json_encode(array('hasil'=>'gagal upload'));
			}
		}
		else {
			echo json_encode(array('hasil'=>'File Kosong..'));
		}
	}

	public function get_dashboard_daily(){
		$params = $_REQUEST;

		$a = '';
		
		$query=$this->m_admin->get_dashboard_daily();
		$total = $this->m_admin->get_total_dashboard_daily_severside($a)->row();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(   
			"recordsTotal"    => $total->total,  
			"recordsFiltered" => $total->total,
			"data"            => $data['data']   // total data array
			);
		echo json_encode($json_data);
	}

	public function do_import_dashboard_monthly(){
		$this->load->library('Spreadsheet_Excel_Reader');

		$today = date("Y-m-d");
		
		if(isset($_FILES)){

			$getflag = $this->m_admin->get_flag_dashboard_monthly()->row_array();
			$flagbefore = $getflag['flag'];
			$flagafter = $getflag['id'];

			if ($flagafter == null) {
				$flagafter = 1;
			}else{
				$flagafter;
			}
			// echo $getflag['flag'];die;

			$data = new Spreadsheet_Excel_Reader($_FILES['file_upload']['tmp_name']);
			$row = 0;
			$col = 0;
			// print_r($_POST); die;
			$berhasil_input = 0;
			for($sheet = 0; $sheet < intval($this->input->post('sheet')); $sheet++)
			{
				$jumlahbaris = $data->rowcount($sheet_index=$sheet);
				$row = 4;
				while ($row <= $jumlahbaris && $data->val($row,1,$sheet)!='') 
				{
					$result = $this->m_admin->do_insert_dashboard_monthly($data->val($row,1,$sheet), $data->val($row,2,$sheet), $data->val($row,3,$sheet), $data->val($row,4,$sheet), $data->val($row,5,$sheet), $data->val($row,6,$sheet), $data->val($row,7,$sheet), $data->val($row,8,$sheet), $data->val($row,9,$sheet), $data->val($row,10,$sheet), $data->val($row,11,$sheet), $data->val($row,12,$sheet), $data->val($row,13,$sheet), $data->val($row,14,$sheet), $data->val($row,15,$sheet), $this->session->name, $flagafter);
					if($result != 1) {
						echo json_encode(array('hasil'=>'gagal upload'));
					}

					if ($result == 1) {
						$berhasil_input++;
					}
					$row ++;
				}
			}
			$total = 'TOTAL';
			$result_total = $this->m_admin->do_insert_dashboard_monthly($total, $data->val(2,2), $data->val(2,3), $data->val(2,4), $data->val(2,5), $data->val(2,6), $data->val(2,7), $data->val(2,8), $data->val(2,9), $data->val(2,10), $data->val(2,11), $data->val(2,12), $data->val(2,13), $data->val(2,14), $data->val(2,15), $this->session->name, $flagafter);

			if ($result_total == 1) {
				$this->m_admin->do_clear_data_dashboard_monthly($flagafter);
				$getlastupload = $this->m_admin->get_flag_dashboard_monthly()->row_array();
				$tgl_upload = $getlastupload['tgl_upload'];
				$uploader = $getlastupload['uploader'];
				echo json_encode(array('hasil'=>'success', 'berhasil_input'=>$berhasil_input, 'tgl_upload'=>$tgl_upload, 'uploader'=>$uploader));
			}else{
				echo json_encode(array('hasil'=>'gagal upload'));
			}
		}
		else {
			echo json_encode(array('hasil'=>'File Kosong..'));
		}
	}

	public function get_dashboard_monthly(){
		$params = $_REQUEST;

		$a = '';
		$query=$this->m_admin->get_dashboard_monthly();
		$total = $this->m_admin->get_total_dashboard_monthly_severside($a)->row();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(   
			"recordsTotal"    => $total->total,  
			"recordsFiltered" => $total->total,
			"data"            => $data['data']   // total data array
			);
		echo json_encode($json_data);
	}

	public function do_update_dashboard_project(){
		$id = $this->input->post('id');
		$column = $this->input->post('column');
		$value = $this->input->post('value');
		$id_project = $this->input->post('id_project');

		$hasil = $this->m_admin->do_update_dashboard_project_percolumn($id, $column, $value);

		if($hasil['affected_rows'] != '-1'){

			//script update total otomadang, bila update total manual comment script dibawah
			$total = $this->m_admin->get_total_dashboard_project($id_project)->row();
			$data = array(
				'active'	=> $total->total_active,
				'revenue'	=> $total->total_revenue,
				'edited_by'	=> $this->session->name
			);
			$updatequery = $this->m_admin->do_update_total_dashboard_project($id_project, $data);
			$query = $this->m_admin->get_dashboard_project_bymonthtotal($id_project)->row();
			$json_data = array(
				'hasil'			=>	'success',
				'total_active'	=>	$query->active,
				'total_revenue'	=>	$query->revenue,
				'last_edited'	=>	$query->last_edited,
				'edited_by'		=>	$query->edited_by,
				'message'		=>	'Update Data "'.$column.'" Success'
				);
			echo json_encode($json_data);
			//==============================================================================

			// echo json_encode(array('hasil'=> 'success', 'message'=>'Update Data '.$column.' Success'));
		}
		else{
			echo json_encode(array('hasil'=> 'error', 'message'=>$hasil['error']['message']));
		}
	}

	public function get_dashboard_region(){
		$params = $_REQUEST;
		$id_project = $params['searchby_id_project'];

		$a = '';
		if($id_project!=''){
			$a.=" AND a.id_project = '".$id_project."'";
		}
		
		$query=$this->m_admin->get_dashboard_region($a);

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(
			"data"  => $data['data']  
			);

		echo json_encode($json_data);
	}

	public function do_import_dashboard_region(){
		$this->load->library('Spreadsheet_Excel_Reader');

		$id_project = $this->input->post('id_project');
		$today = date("Y-m-d");

		if(isset($_FILES)){

			$getflag = $this->m_admin->get_flag_dashboard_region()->row_array();
			$flagbefore = $getflag['flag'];
			$flagafter = $getflag['id'];

			if ($flagafter == null) {
				$flagafter = 1;
			}else{
				$flagafter;
			}
			// echo $getflag['flag'];die;

			$data = new Spreadsheet_Excel_Reader($_FILES['file_upload']['tmp_name']);
			$row = 0;
			$col = 0;
			// print_r($_POST); die;
			$berhasil_input = 0;
			for($sheet = 0; $sheet < intval($this->input->post('sheet')); $sheet++)
			{
				$jumlahbaris = $data->rowcount($sheet_index=$sheet);
				$row = 3;
				while ($row <= $jumlahbaris && $data->val($row,1,$sheet)!='') 
				{
					$result = $this->m_admin->do_insert_dashboard_region($data->val($row,1,$sheet), $data->val($row,2,$sheet), $data->val($row,3,$sheet), $data->val($row,4,$sheet), $data->val($row,5,$sheet), $data->val($row,6,$sheet), $data->val($row,7,$sheet), $data->val($row,8,$sheet), $data->val($row,9,$sheet), $data->val($row,10,$sheet), $data->val($row,11,$sheet), $data->val($row,12,$sheet), $data->val($row,13,$sheet), $data->val($row,14,$sheet), $this->session->name, $flagafter, $id_project);
					if($result != 1) {
						echo json_encode(array('hasil'=>'gagal upload'));
					}

					if ($result == 1) {
						$berhasil_input++;
					}
					$row ++;
				}
			}
			// $total = 'TOTAL';
			// $result_total = $this->m_admin->do_insert_dashboard_monthly($total, $data->val(2,2), $data->val(2,3), $data->val(2,4), $data->val(2,5), $data->val(2,6), $data->val(2,7), $data->val(2,8), $data->val(2,9), $data->val(2,10), $data->val(2,11), $data->val(2,12), $data->val(2,13), $data->val(2,14), $data->val(2,15), $this->session->name, $flagafter);

			// if ($result_total == 1) {
				$this->m_admin->do_clear_data_dashboard_region($flagafter,$id_project);
				$getlastupload = $this->m_admin->get_flag_dashboard_region()->row_array();
				$tgl_upload = $getlastupload['tgl_upload'];
				$uploader = $getlastupload['uploader'];
				echo json_encode(array('hasil'=>'success', 'berhasil_input'=>$berhasil_input, 'tgl_upload'=>$tgl_upload, 'uploader'=>$uploader));
			// }else{
			// 	echo json_encode(array('hasil'=>'gagal upload'));
			// }
		}
		else {
			echo json_encode(array('hasil'=>'File Kosong..'));
		}
	}

}
