<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tl_spv extends CI_Controller {

	function __construct() {
        parent::__construct();
		// if ($this->session->logged_in != TRUE) {
		// 	redirect('login');
		// }
		if(!$this->session->logged_in) redirect('login');
    }

	// public function home()
	// {
	// 	$this->load->view('master');
	// }

	public function home()
	{
		$this->load->model('m_dashboard');
		$row = $this->m_dashboard->get_total_call()->row_array();
		$data = array(
			'data'			=> $row,
			'page' 			=> 'home',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> 'tl_spv'
		);
		$this->load->view('master', $data);
	}

	public function single()
	{
		$data = array(
			'page' 			=> 'single',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> 'tl_spv'
		);
		$this->load->view('master', $data);
	}

	public function bulk()
	{
		$data = array(
			'page'			=> 'bulk',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> 'tl_spv'
		);
		$this->load->view('master', $data);
	}

	public function takers()
	{
		$data = array(
			'page'			=> 'takers',
			'sidebar' 		=> 'sidebar',
			'user_level' 	=> 'tl_spv'
		);
		$this->load->view('master', $data);
	}
}
