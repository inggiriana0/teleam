<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_dashboard');
	}

	public function tes()
	{
		$data = array(
			'page' 			=> 'daily'
		);
		$this->load->view('tesgrafik', $data);
	}

	public function summary()
	{
		// $this->load->library('../controllers/whathever');
		$row = $this->m_dashboard->get_dashboard_summary()->row_array();
		$data = array(
			'data'			=> $row,
			'page' 			=> 'summary'
		);
		$this->load->view('dashboard', $data);
	}

	public function daily()
	{
		$this->load->model('m_admin');
		$row = $this->m_admin->get_flag_dashboard_daily()->row_array();
		$data = array(
			'data'			=> $row,
			'page' 			=> 'daily'
		);
		$this->load->view('dashboard', $data);
	}

	public function monthly()
	{
		$this->load->model('m_admin');
		$row = $this->m_admin->get_flag_dashboard_monthly()->row_array();
		$data = array(
			'data'			=> $row,
			'page' 			=> 'monthly'
		);
		$this->load->view('dashboard', $data);
	}

	public function project()
	{
		$data = array(
			'page' 			=> 'project'
		);
		$this->load->view('dashboard', $data);
	}

	public function region()
	{
		$data = array(
			'page' 			=> 'region'
		);
		$this->load->view('dashboard', $data);
	}

	public function best_performance()
	{
		$data = array(
			'page' 			=> 'best_performance'
		);
		$this->load->view('dashboard', $data);
	}

	public function get_dashboard_single()
	{
		$jenis_wl = $this->input->post('jenis_wl');
		$a = '';
		if ($jenis_wl != '0') {
			$a .= " AND a.jenis_wl = '" . $jenis_wl . "'";
		}
		$query = $this->m_dashboard->get_dashboard_single($a)->row_array();
		$data = $query;
		echo json_encode($data);
	}

	public function get_dashboard_bulk()
	{
		// $id_tb_user = $this->input->post('id_tb_user');
		$query = $this->m_dashboard->get_dashboard_bulk()->row_array();
		$data = $query;
		echo json_encode($data);
	}

	public function get_dashboard_takers()
	{
		$jenis_wl = $this->input->post('jenis_wl');
		$a = '';
		if ($jenis_wl != '0') {
			$a .= " AND a.jenis_wl = '" . $jenis_wl . "'";
		}
		$query = $this->m_dashboard->get_dashboard_takers($a)->row_array();
		$data = $query;
		echo json_encode($data);
	}

	public function get_dashboard_daily_for_chart()
	{
		$data['data']= array();
		$query=$this->m_dashboard->get_dashboard_daily_for_chart();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}
		echo json_encode($data);
	}

	public function get_dashboard_daily()
	{
		$query=$this->m_dashboard->get_dashboard_daily();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(   
			"data"            => $data['data']   // total data array
			);
		echo json_encode($json_data);
	}

	public function get_dashboard_monthly_for_chart()
	{
		$data['data']= array();
		$query=$this->m_dashboard->get_dashboard_monthly_for_chart();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i]= $myRow;
			$i++;
		}
		echo json_encode($data);
	}

	public function get_dashboard_monthly()
	{
		$query=$this->m_dashboard->get_dashboard_monthly();

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$json_data = array(   
			"data"            => $data['data']   // total data array
			);
		echo json_encode($json_data);
	}

	public function get_dashboard_project()
	{
		$params = $_REQUEST;
		$id_project = $params['id_project'];
		
		$query=$this->m_dashboard->get_dashboard_project($id_project);

		$data['data']= array();
		$i=0;
		foreach ($query->result_array() as $myRow) {
			$data['data'][$i] = $myRow;
			$i++;
		}

		$this->load->model('m_admin');
		$query = $this->m_admin->get_dashboard_project_bymonthtotal($id_project)->row();

		$json_data = array(   
			"data"           => $data['data'],
			'total_active'	=>	$query->active,
			'total_revenue'	=>	$query->revenue,
			'last_edited'	=>	$query->last_edited,
			'edited_by'		=>	$query->edited_by 
			);
		echo json_encode($json_data);
	}
}
