<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_takers extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		// $this->load->database();
		$this->db1 = $this->load->database('db_teleam', TRUE);
    }

	function do_insert($data){
		// $this->db1->insert('single', $data);
		// return ($this->db1->affected_rows() != 1) ? false : true;
		$query = $this->db1->insert('takers', $data);
		return array('success' => $query, 
					'error' => $this->db1->error());
		// return array('success' => $this->db1->affected_rows(), 
		// 			'error' => $this->db1->error());
	}

	function do_update($data,$id_bulk){
		$this->db1->where('id', $id_bulk);
		$this->db1->update('takers', $data);
		return array('success' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function get_takers_severside($order='',$direction='',$start_limit='',$end_limit='',$search=''){
		$sqlrec =  " ORDER BY ". $order."   ".$direction."  LIMIT ".$start_limit." , ".$end_limit." ";
		$sql = "SELECT a.*, b.status_call as name_status_call, c.reason_call as name_reason_call, d.subreason_call as name_subreason_call
				FROM takers a
				LEFT JOIN takers_status_call b ON a.status_call=b.id
				LEFT JOIN takers_reason_call c ON a.reason_call=c.id
				LEFT JOIN takers_subreason_call d ON a.subreason_call=d.id
				WHERE 1 ".$search.$sqlrec;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_takers_severside($search=''){
		$sql = "SELECT count(*) as total
				FROM takers a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_takers_export($search=''){
		$sql = "SELECT a.*, b.status_call as name_status_call, c.reason_call as name_reason_call, d.subreason_call as name_subreason_call
				FROM takers a
				LEFT JOIN takers_status_call b ON a.status_call=b.id
				LEFT JOIN takers_reason_call c ON a.reason_call=c.id
				LEFT JOIN takers_subreason_call d ON a.subreason_call=d.id
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_takers_byid($id_takers){
		$sql = "SELECT a.*, b.status_call as name_status_call, c.reason_call as name_reason_call, d.subreason_call as name_subreason_call
				FROM takers a
				LEFT JOIN takers_status_call b ON a.status_call=b.id
				LEFT JOIN takers_reason_call c ON a.reason_call=c.id
				LEFT JOIN takers_subreason_call d ON a.subreason_call=d.id
				WHERE a.id = ?";
		$query = $this->db1->query($sql,array($id_takers));
		return $query;
	}

	function get_jenis_wl(){
		$sql = "SELECT *
				FROM takers_jenis_wl";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_jam_call(){
		$sql = "SELECT *
				FROM takers_jam_call";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_prt(){
		$sql = "SELECT *
				FROM takers_prt";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_arpu(){
		$sql = "SELECT *
				FROM takers_arpu";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_status_call(){
		$sql = "SELECT *
				FROM takers_status_call";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_reason_call($id_status_call){
		$sql = "SELECT *
				FROM takers_reason_call a
				WHERE id_status_call = ?";
		$query = $this->db1->query($sql,array($id_status_call));
		return $query;
	}

	function get_subreason_call($id_reason_call){
		$sql = "SELECT *
				FROM takers_subreason_call a
				WHERE id_reason_call = ?";
		$query = $this->db1->query($sql,array($id_reason_call));
		return $query;
	}

	function get_paket_penawaran(){
		$sql = "SELECT *
				FROM takers_paket_penawaran";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_wl_source(){
		$sql = "SELECT *
				FROM takers_wl_source";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_region(){
		$sql = "SELECT *
				FROM takers_region";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_project(){
		$sql = "SELECT *
				FROM takers_project";
		$query = $this->db1->query($sql,array());
		return $query;
	}


}
?>