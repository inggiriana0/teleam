<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		// $this->load->database();
		$this->db1 = $this->load->database('db_teleam', TRUE);
    }

	function get_dashboard_single($jenis_wl=''){
		$sql = "SELECT 
					(CASE WHEN a.total_call > 0 THEN a.total_call ELSE 0 END) as total_call,
					(CASE WHEN b.agree > 0 THEN b.agree ELSE 0 END) as agree,
					(CASE WHEN c.disagree > 0 THEN c.disagree ELSE 0 END) as disagree,
					(CASE WHEN d.follow_up > 0 THEN d.follow_up ELSE 0 END) as follow_up,
					(CASE WHEN e.busy > 0 THEN e.busy ELSE 0 END) as busy,
					(CASE WHEN f.rna > 0 THEN f.rna ELSE 0 END) as rna,
					(CASE WHEN g.voicemail > 0 THEN g.voicemail ELSE 0 END) as voicemail,
					(CASE WHEN h.contacted > 0 THEN h.contacted ELSE 0 END) as contacted,
					(CASE WHEN i.not_contacted > 0 THEN i.not_contacted ELSE 0 END) as not_contacted,
					(CASE WHEN j.not_contactable > 0 THEN j.not_contactable ELSE 0 END) as not_contactable,
					h.contacted/a.total_call as contacted_rate,
					b.agree/h.contacted as conversion_rate,
					(CASE WHEN k.revenue > 0 THEN k.revenue ELSE 0 END) as revenue
				FROM
				(SELECT COUNT(*) as total_call FROM single a 
					WHERE MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") a,
				(SELECT COUNT(*) as agree FROM single a 
					WHERE reason_call = 1
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") b,
				(SELECT COUNT(*) as disagree FROM single a 
					WHERE reason_call = 2
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") c,
				(SELECT COUNT(*) as follow_up FROM single a 
					WHERE reason_call = 3
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") d,
				(SELECT COUNT(*) as busy FROM single a 
					WHERE reason_call = 4
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") e,
				(SELECT COUNT(*) as rna FROM single a 
					WHERE reason_call = 5
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") f,
				(SELECT COUNT(*) as voicemail FROM single a 
					WHERE reason_call = 6
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") g,
				(SELECT COUNT(*) as contacted FROM single a 
					WHERE status_call = 1
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") h,
				(SELECT COUNT(*) as not_contacted FROM single a 
					WHERE status_call = 2
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") i,
				(SELECT COUNT(*) as not_contactable FROM single a 
					WHERE status_call = 3
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") j,
				(SELECT 0 as revenue) k
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_bulk(){
		$sql = "SELECT 
					(CASE WHEN a.total_call > 0 THEN a.total_call ELSE 0 END) as total_call,
					(CASE WHEN b.agree > 0 THEN b.agree ELSE 0 END) as agree,
					(CASE WHEN c.disagree > 0 THEN c.disagree ELSE 0 END) as disagree,
					(CASE WHEN d.follow_up > 0 THEN d.follow_up ELSE 0 END) as follow_up,
					(CASE WHEN e.busy > 0 THEN e.busy ELSE 0 END) as busy,
					(CASE WHEN f.rna > 0 THEN f.rna ELSE 0 END) as rna,
					(CASE WHEN g.voicemail > 0 THEN g.voicemail ELSE 0 END) as voicemail,
					(CASE WHEN h.contacted > 0 THEN h.contacted ELSE 0 END) as contacted,
					(CASE WHEN i.not_contacted > 0 THEN i.not_contacted ELSE 0 END) as not_contacted,
					(CASE WHEN j.not_contactable > 0 THEN j.not_contactable ELSE 0 END) as not_contactable,
					h.contacted/a.total_call as contacted_rate,
					b.agree/h.contacted as conversion_rate,
					(CASE WHEN k.revenue > 0 THEN k.revenue ELSE 0 END) as revenue
				FROM
				(SELECT COUNT(*) as total_call FROM bulk a 
					WHERE MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) a,
				(SELECT COUNT(*) as agree FROM bulk a 
					WHERE reason_call = 1
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) b,
				(SELECT COUNT(*) as disagree FROM bulk a 
					WHERE reason_call = 2
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) c,
				(SELECT COUNT(*) as follow_up FROM bulk a 
					WHERE reason_call = 3
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) d,
				(SELECT COUNT(*) as busy FROM bulk a 
					WHERE reason_call = 4
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) e,
				(SELECT COUNT(*) as rna FROM bulk a 
					WHERE reason_call = 5
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) f,
				(SELECT COUNT(*) as voicemail FROM bulk a 
					WHERE reason_call = 6
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) g,
				(SELECT COUNT(*) as contacted FROM bulk a 
					WHERE status_call = 1
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) h,
				(SELECT COUNT(*) as not_contacted FROM bulk a 
					WHERE status_call = 2
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) i,
				(SELECT COUNT(*) as not_contactable FROM bulk a 
					WHERE status_call = 3
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) j,
				(SELECT 0 as revenue) k
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_takers($jenis_wl=''){
		$sql = "SELECT 
					(CASE WHEN a.total_call > 0 THEN a.total_call ELSE 0 END) as total_call,
					(CASE WHEN b.agree > 0 THEN b.agree ELSE 0 END) as agree,
					(CASE WHEN c.disagree > 0 THEN c.disagree ELSE 0 END) as disagree,
					(CASE WHEN d.follow_up > 0 THEN d.follow_up ELSE 0 END) as follow_up,
					(CASE WHEN e.busy > 0 THEN e.busy ELSE 0 END) as busy,
					(CASE WHEN f.rna > 0 THEN f.rna ELSE 0 END) as rna,
					(CASE WHEN g.voicemail > 0 THEN g.voicemail ELSE 0 END) as voicemail,
					(CASE WHEN h.contacted > 0 THEN h.contacted ELSE 0 END) as contacted,
					(CASE WHEN i.not_contacted > 0 THEN i.not_contacted ELSE 0 END) as not_contacted,
					(CASE WHEN j.not_contactable > 0 THEN j.not_contactable ELSE 0 END) as not_contactable,
					h.contacted/a.total_call as contacted_rate,
					b.agree/h.contacted as conversion_rate,
					(CASE WHEN k.revenue > 0 THEN k.revenue ELSE 0 END) as revenue
				FROM
				(SELECT COUNT(*) as total_call FROM takers a 
					WHERE MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") a,
				(SELECT COUNT(*) as agree FROM takers a 
					WHERE reason_call = 1
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") b,
				(SELECT COUNT(*) as disagree FROM takers a 
					WHERE reason_call = 2
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") c,
				(SELECT COUNT(*) as follow_up FROM takers a 
					WHERE reason_call = 3
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") d,
				(SELECT COUNT(*) as busy FROM takers a 
					WHERE reason_call = 4
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") e,
				(SELECT COUNT(*) as rna FROM takers a 
					WHERE reason_call = 5
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") f,
				(SELECT COUNT(*) as voicemail FROM takers a 
					WHERE reason_call = 6
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") g,
				(SELECT COUNT(*) as contacted FROM takers a 
					WHERE status_call = 1
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") h,
				(SELECT COUNT(*) as not_contacted FROM takers a 
					WHERE status_call = 2
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") i,
				(SELECT COUNT(*) as not_contactable FROM takers a 
					WHERE status_call = 3
						AND MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())".$jenis_wl.") j,
				(SELECT 0 as revenue) k
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_call(){
		$sql = "SELECT 
					(CASE WHEN a.single > 0 THEN a.single ELSE 0 END) as total_single,
					(CASE WHEN b.bulk > 0 THEN b.bulk ELSE 0 END) as total_bulk,
					(CASE WHEN c.takers > 0 THEN c.takers ELSE 0 END) as total_takers
				FROM
				(SELECT COUNT(*) as single FROM single a 
					WHERE MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) a,
				(SELECT COUNT(*) as bulk FROM bulk a 
					WHERE MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) b,
				(SELECT COUNT(*) as takers FROM takers a 
					WHERE MONTH(created) = MONTH(CURRENT_DATE()) AND YEAR(created) = YEAR(CURRENT_DATE())) c
				";
		$query = $this->db1->query($sql,array($this->session->nik_csdm,$this->session->nik_csdm,$this->session->nik_csdm));
		return $query;
	}

	function get_dashboard_summary(){
		$sql = "SELECT * FROM dashboard_summary
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_daily(){
		$sql = "SELECT * FROM dashboard_daily
				WHERE tgl != 'TOTAL'
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_daily_for_chart(){
		$sql = "SELECT agent as label,
					revenue, target
				FROM dashboard_daily
				WHERE tgl != 'TOTAL' ";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_monthly(){
		$sql = "SELECT * FROM dashboard_monthly
				WHERE bln != 'TOTAL'
				";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_monthly_for_chart(){
		$sql = "SELECT agent as label,
					revenue, target
				FROM dashboard_monthly
				WHERE bln != 'TOTAL' ";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_dashboard_project($id_project){
		$sql = "SELECT * FROM dashboard_project
				WHERE id_project = ?
				";
		$query = $this->db1->query($sql,array($id_project));
		return $query;
	}

}
?>