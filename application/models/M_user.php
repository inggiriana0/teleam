<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		// $this->load->database();
		$this->db1 = $this->load->database('db_teleam', TRUE);
    }

	function get_user($username='',$password=''){
		$sql = "SELECT *
				FROM tb_user a
				WHERE a.nik_csdm = ? AND a.password = ?";
		$query = $this->db1->query($sql,array($username, $password));
		return $query;
	}

	function do_update_is_login_user($id=''){
		$sql = "UPDATE tb_user a
				SET a.is_login = 1, a.last_login = NOW()
				WHERE a.id = ? ";
		$query = $this->db1->query($sql,array($id));
		return $query;
	}

	function do_update_is_logout_user($id=''){
		$sql = "UPDATE tb_user a
				SET a.is_login = 0
				WHERE a.id = ? ";
		$query = $this->db1->query($sql,array($id));
		return $query;
	}


}
?>