<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_single extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		// $this->load->database();
		$this->db1 = $this->load->database('db_teleam', TRUE);
    }

	function do_insert($data){
		// $this->db1->insert('single', $data);
		// return ($this->db1->affected_rows() != 1) ? false : true;
		$query = $this->db1->insert('single', $data);
		return array('success' => $query, 
					'error' => $this->db1->error());
		// return array('success' => $this->db1->affected_rows(), 
		// 			'error' => $this->db1->error());
	}

	function do_update($data,$id_single){
		$this->db1->where('id', $id_single);
		$this->db1->update('single', $data);
		return array('success' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function get_single_severside($order='',$direction='',$start_limit='',$end_limit='',$search=''){
		$sqlrec =  " ORDER BY ". $order."   ".$direction."  LIMIT ".$start_limit." , ".$end_limit." ";
		$sql = "SELECT a.*, b.status_call as name_status_call, c.reason_call as name_reason_call, d.subreason_call as name_subreason_call
				FROM single a
				LEFT JOIN single_status_call b ON a.status_call=b.id
				LEFT JOIN single_reason_call c ON a.reason_call=c.id
				LEFT JOIN single_subreason_call d ON a.subreason_call=d.id
				WHERE 1 ".$search.$sqlrec;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_single_severside($search=''){
		$sql = "SELECT count(*) as total
				FROM single a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_single_export($search=''){
		$sql = "SELECT a.*, b.status_call as name_status_call, c.reason_call as name_reason_call, d.subreason_call as name_subreason_call
				FROM single a
				LEFT JOIN single_status_call b ON a.status_call=b.id
				LEFT JOIN single_reason_call c ON a.reason_call=c.id
				LEFT JOIN single_subreason_call d ON a.subreason_call=d.id
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_single_byid($id_single){
		$sql = "SELECT a.*, b.status_call as name_status_call, c.reason_call as name_reason_call, d.subreason_call as name_subreason_call
				FROM single a
				LEFT JOIN single_status_call b ON a.status_call=b.id
				LEFT JOIN single_reason_call c ON a.reason_call=c.id
				LEFT JOIN single_subreason_call d ON a.subreason_call=d.id
				WHERE a.id = ?";
		$query = $this->db1->query($sql,array($id_single));
		return $query;
	}

	function get_total_home_useragent(){
		$sql = "SELECT 
					(CASE WHEN a.single > 0 THEN a.single ELSE 0 END) as total_single_today,
					(CASE WHEN b.bulk > 0 THEN b.bulk ELSE 0 END) as total_bulk_today,
					(CASE WHEN c.takers > 0 THEN c.takers ELSE 0 END) as total_takers_today
				FROM
				(SELECT COUNT(*) as single FROM single a 
					WHERE DATE_FORMAT(a.created, '%Y-%m-%d') = CURDATE() AND a.nik_csdm = ?) a,
				(SELECT COUNT(*) as bulk FROM bulk a 
					WHERE DATE_FORMAT(a.created, '%Y-%m-%d') = CURDATE() AND a.nik_csdm = ?) b,
				(SELECT COUNT(*) as takers FROM takers a 
					WHERE DATE_FORMAT(a.created, '%Y-%m-%d') = CURDATE() AND a.nik_csdm = ?) c
				";
		$query = $this->db1->query($sql,array($this->session->nik_csdm,$this->session->nik_csdm,$this->session->nik_csdm));
		return $query;
	}

	function get_jenis_wl(){
		$sql = "SELECT *
				FROM single_jenis_wl";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_jam_call(){
		$sql = "SELECT *
				FROM single_jam_call";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_prt(){
		$sql = "SELECT *
				FROM single_prt";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_arpu(){
		$sql = "SELECT *
				FROM single_arpu";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_status_call(){
		$sql = "SELECT *
				FROM single_status_call";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_status_call2($status_call){
        $this->db1->like('status_call', $status_call , 'both');
        $query = $this->db1->get('single_status_call')->result();
        $response = array();
        foreach($query as $row ){
        	$response[] = array(
           		"id"=>$row->id,
           		"text"=>$row->status_call
           	);
        }       
        return $response; 
    }

	function get_reason_call($id_status_call){
		$sql = "SELECT *
				FROM single_reason_call a
				WHERE id_status_call = ?";
		$query = $this->db1->query($sql,array($id_status_call));
		return $query;
	}

	function get_reason_call2($id_status_call){
        $this->db1->like('reason_call', $reason_call , 'both');
        // $this->db1->order_by('name', 'ASC');
        // $this->db1->limit(10);
		$this->db1->where('id_status_call', $id_status_call);
        $query = $this->db1->get('single_reason_call')->result();
        $response = array();
        foreach($query as $row ){
        	$response[] = array(
           		"id"=>$row->id,
           		"text"=>$row->reason_call
           	);
        }       
        return $response; 
    }

	function get_subreason_call($id_reason_call){
		$sql = "SELECT *
				FROM single_subreason_call a
				WHERE id_reason_call = ?";
		$query = $this->db1->query($sql,array($id_reason_call));
		return $query;
	}

	function get_paket_aktif(){
		$sql = "SELECT *
				FROM single_paket_aktif";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_wl_source(){
		$sql = "SELECT *
				FROM single_wl_source";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_region(){
		$sql = "SELECT *
				FROM single_region";
		$query = $this->db1->query($sql,array());
		return $query;
	}


}
?>