<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {
	
	var $db1;

	function __construct() {
        parent::__construct();
		// $this->load->database();
		$this->db1 = $this->load->database('db_teleam', TRUE);
    }

	function do_insert($data){
		$query = $this->db1->insert('tb_user', $data);
		return array('success' => $query, 
					'error' => $this->db1->error());
	}

	function do_update($data,$id_tb_user){
		$this->db1->where('id', $id_tb_user);
		$this->db1->update('tb_user', $data);
		return array('success' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function get_tb_user_severside($order='',$direction='',$start_limit='',$end_limit='',$search=''){
		$sqlrec =  " ORDER BY ". $order."   ".$direction."  LIMIT ".$start_limit." , ".$end_limit." ";
		$sql = "SELECT a.*
				FROM tb_user a
				WHERE 1 ".$search.$sqlrec;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_tb_user_severside($search=''){
		$sql = "SELECT count(*) as total
				FROM tb_user a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_tb_user_export($search=''){
		$sql = "SELECT a.*
				FROM tb_user a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_tb_user_byid($id_tb_user){
		$sql = "SELECT a.*
				FROM tb_user a
				WHERE a.id = ?";
		$query = $this->db1->query($sql,array($id_tb_user));
		return $query;
	}

	function do_update_dashboard_summary($data,$id_summary){
		$this->db1->where('id', $id_summary);
		$this->db1->update('dashboard_summary', $data);
		return array('success' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function get_flag_dashboard_daily(){
		$sql = "SELECT *
				FROM dashboard_daily ORDER BY id DESC";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function do_insert_dashboard_daily($tgl, $agent, $call, $contacted, $agree, $active, $revenue, $growth_dod, $contacted_rate, $conversion_rate, $active_rate, $target, $ach, $uploader, $flag){
		$sql = "INSERT INTO dashboard_daily (tgl,agent,`call`,contacted,agree,active,revenue,growth_dod,contacted_rate,conversion_rate,active_rate,target,ach,uploader,flag)
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db1->query($sql,array($tgl, $agent, $call, $contacted, $agree, $active, $revenue, $growth_dod, $contacted_rate, $conversion_rate, $active_rate, $target, $ach, $uploader, $flag));
		return $query;
	}

	function do_clear_data_dashboard_daily($flag){
		$sql = "DELETE FROM dashboard_daily 
				WHERE flag != ?";
		$query = $this->db1->query($sql,array($flag));
		return $query;
	}

	function get_dashboard_daily(){
		$sql = "SELECT a.*
				FROM dashboard_daily a";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_dashboard_daily_severside($search=''){
		$sql = "SELECT count(*) as total
				FROM dashboard_daily a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_flag_dashboard_monthly(){
		$sql = "SELECT *
				FROM dashboard_monthly ORDER BY id DESC";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function do_insert_dashboard_monthly($bln, $agent, $call, $contacted, $agree, $active, $revenue, $rev_mom, $contacted_rate, $conversion_rate, $conversion_rate_psb, $conversion_rate_uplift, $active_rate, $target, $ach, $uploader, $flag){
		$sql = "INSERT INTO dashboard_monthly (bln,agent,`call`,contacted,agree,active,revenue,rev_mom,contacted_rate,conversion_rate,conversion_rate_psb,conversion_rate_uplift,active_rate,target,ach,uploader,flag)
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db1->query($sql,array($bln, $agent, $call, $contacted, $agree, $active, $revenue, $rev_mom, $contacted_rate, $conversion_rate, $conversion_rate_psb, $conversion_rate_uplift, $active_rate, $target, $ach, $uploader, $flag));
		return $query;
	}

	function do_clear_data_dashboard_monthly($flag){
		$sql = "DELETE FROM dashboard_monthly 
				WHERE flag != ?";
		$query = $this->db1->query($sql,array($flag));
		return $query;
	}

	function get_dashboard_monthly(){
		$sql = "SELECT a.*
				FROM dashboard_monthly a";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_total_dashboard_monthly_severside($search=''){
		$sql = "SELECT count(*) as total
				FROM dashboard_monthly a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function do_update_dashboard_project_percolumn($id, $column, $value){
		$this->db1->set($column, $value);
		$this->db1->where('id', $id);
		$this->db1->update('dashboard_project');
		return array('affected_rows' => $this->db1->affected_rows(), 
					'error' => $this->db1->error());
	}

	function get_total_dashboard_project($id_project){
		$sql = "SELECT sum(active) as total_active, sum(revenue) as total_revenue
				FROM dashboard_project a
				WHERE a.id_project = ? AND a.month != 'TOTAL'";
		$query = $this->db1->query($sql,array($id_project));
		return $query;
	}

	function get_dashboard_project_bymonthtotal($id_project){
		$sql = "SELECT a.*
				FROM dashboard_project a
				WHERE a.id_project = ? AND a.month = 'TOTAL' ";
		$query = $this->db1->query($sql,array($id_project));
		return $query;
	}

	function do_update_total_dashboard_project($id_project, $data){
		$this->db1->where('id_project', $id_project);
		$this->db1->where('month', 'TOTAL');
		$this->db1->update('dashboard_project', $data);
		return array('success' => $this->db1->affected_rows(),
					'error' => $this->db1->error());
	}

	function get_dashboard_region($search=''){
		$sql = "SELECT a.*
				FROM dashboard_region a
				WHERE 1 ".$search;
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function get_flag_dashboard_region(){
		$sql = "SELECT *
				FROM dashboard_region ORDER BY id DESC";
		$query = $this->db1->query($sql,array());
		return $query;
	}

	function do_insert_dashboard_region($region, $total, $januari, $februari, $maret, $april, $mei, $juni, $juli, $agustus, $september, $oktober, $november, $desember, $uploader, $flag, $id_project){
		$sql = "INSERT INTO dashboard_region (region,total,januari,februari,maret,april,mei,juni,juli,agustus,september,oktober,november,desember,uploader,flag,id_project)
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db1->query($sql,array($region, $total, $januari, $februari, $maret, $april, $mei, $juni, $juli, $agustus, $september, $oktober, $november, $desember, $uploader, $flag, $id_project));
		return $query;
	}

	function do_clear_data_dashboard_region($flag,$id_project){
		$sql = "DELETE FROM dashboard_region 
				WHERE flag != ? AND id_project = ?";
		$query = $this->db1->query($sql,array($flag,$id_project));
		return $query;
	}

}
?>