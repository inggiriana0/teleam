<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard Region</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<!-- /.content-header -->

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- card -->
            <div class="card card-secondary card-outline" id="card_form">
              <div class="card-header">
                <h3 class="card-title" id="card_title">Upload File</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <form class="form-horizontal text-sm" id="region_form">
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">PROJECT</span>
                      </div>
                      <select class="form-control form-control-sm text-sm select2_project" id="id_project" name="id_project" required>
                        <option value="1">RED SOCIETY</option>
                        <option value="2">EDUCATION</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-2" style="visibility: hidden;">
                    <div class="input-group mb-3">
                      <button type="button" class="btn btn-danger btn-sm" id="button_load_data">Load</button>
                    </div>
                  </div>
                  <div class="col-sm-7" style="visibility: hidden;"><label>hide</label></div>
                </div>
                <div class="form-group-sm row">
                  <div class="col-sm-12">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="form-line">
                        <!-- <input type="file" class="custom-file-input" id="file_upload" name="file_upload"> -->
                        <input type="file" id="file_upload" class="form-control" placeholder="" name="file_upload">
                        <!-- <label class="custom-file-label" for="exampleInputFile">Choose file</label> -->
                      </div>
                      <div class="input-group-append">
                        <button type="submit" class="input-group-text" id="button_doupload"><i class='fas fa-cloud-upload-alt'></i>&nbsp;UPLOAD</button>
                      </div>
                    </div>
                    <div class="help-info" style="color:red">*Ket: Format file harus sama dengan contoh yang disediakan</div>
                  </div>
                </div>
                <br>
                <div class="form-group-sm row">
                  <div class="col-sm-12">
                    <a href="<?php echo base_url(); ?>uploads/format_upload_dashboard_region.xls" class="btn btn-success m-t-15 waves-effect" id="a_doupload"><i class='fas fa-file-excel'></i>&nbsp;Format Excel</a>
                    <!-- <div class="help-info" style="color:red">*Ket: Kolom revenue & kolom target isi dengan angka tanpa titik/koma</div> -->
                  </div>              
                </div>
                <div class="form-group" style="display: none;">
                  <div class="input-group spinner" data-trigger="spinner">
                    <div class="form-line">
                      <input type="text" class="form-control text-center" value="1" data-rule="quantity" id="sheet" nama="sheet" data-max="10">
                    </div>
                    <div class="help-info">Jumlah Sheet</div>
                    <span class="input-group-addon">
                      <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                      <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                    </span>
                  </div>
                </div>
              </form>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->

             <!-- card -->
            <div class="card card-secondary text-sm card-outline">
              <div class="card-header">
                <h3 class="card-title">Data Dashboard Region</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Uploaded</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="tgl_upload" name="tgl_upload" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Uploaded By</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="uploader" name="uploader" readonly>
                    </div>
                  </div>
                </div>
                
                <table id="table_region" class="table table-bordered table-striped table-d_region">
                  <thead>
                    <tr>
                      <th>Region</th>
                      <th>Total</th>
                      <th>Januari</th>
                      <th>Februari</th>
                      <th>Maret</th>
                      <th>April</th>
                      <th>Mei</th>
                      <th>Juni</th>
                      <th>Juli</th>
                      <th>Agustus</th>
                      <th>September</th>
                      <th>Oktober</th>
                      <th>November</th>
                      <th>Desember</th>
                    </tr>
                  </thead>
                  <!-- <tfoot>
                    <tr>
                      <th>NIK CSDM</th>
                      <th>NAME</th>
                      <th>SITE</th>
                      <th>USER LEVEL</th>
                      <th>LAST LOGIN</th>
                      <th>ACTION</th>
                    </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 