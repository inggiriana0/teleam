<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">USER DATA</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<!-- /.content-header -->

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- card -->
            <div class="card card-success" id="card_form" style="display: none;">
              <div class="card-header">
                <h3 class="card-title" id="card_title">EDIT USER FORM</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <form class="form-horizontal text-sm" id="userdata_form">
                <input type="hidden" name="id_tb_user" id="id_tb_user" value="0" />
                <div class="form-group-sm row">
                  <label for="nik_csdm" class="col-sm-2 col-form-label">NIK CSDM</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nik_csdm" name="nik_csdm">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="name" class="col-sm-2 col-form-label">NAME</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="name" name="name">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="leader" class="col-sm-2 col-form-label">TEAM LEADER</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="leader" name="leader">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="jabatan" class="col-sm-2 col-form-label">JABATAN</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_jabatan" id="jabatan" name="jabatan" data-placeholder="--- JABATAN ---" required>
                      <option value='AGENT TELEAM'>AGENT TELEAM</option>
                      <option value='TL'>LEADER</option>
                      <option value='SPV'>SUPERVISOR</option>
                      <option value='CAMPAIGN OFFICER'>CAMPAIGN OFFICER</option>
                      <option value='KOORDINATOR'>KOORDINATOR</option>
                      <option value='ADMIN'>ADMIN</option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="site" class="col-sm-2 col-form-label">SITE</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_site" id="site" name="site" data-placeholder="--- SITE ---" required>
                      <option value='BANDUNG'>BANDUNG</option>
                      <option value='MAKASSAR'>MAKASSAR</option>
                      <option value='NASIONAL'>NASIONAL</option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="gender" class="col-sm-2 col-form-label">GENDER</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_gender" id="gender" name="gender" data-placeholder="--- GENDER ---" required>
                      <option value='L'>L</option>
                      <option value='P'>P</option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="user_level" class="col-sm-2 col-form-label">USER LEVEL</label>
                  <div class="col-sm-10">
                    <!-- <input type="text" class="form-control form-control-sm text-sm" name="temp_user_level" id="temp_user_level" value=""/> -->
                    <select class="form-control form-control-sm text-sm select2_user_level" id="user_level" name="user_level" data-placeholder="--- USER LEVEL ---" required>
                      <option value='AGENT'>AGENT</option>
                      <option value='TL'>TL</option>
                      <option value='SPV'>SPV</option>
                      <option value='ADMIN'>ADMIN</option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="user_level" class="col-sm-2 col-form-label">USER STATUS</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_status_user" id="status_user" name="status_user" data-placeholder="--- GENDER ---" required>
                      <option value='1'>AKTIF</option>
                      <option value='0'>NON AKTIF</option>
                    </select>
                  </div>
                </div>
                <br>
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Date Created</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="created" name="created" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Created By</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="input_by" name="input_by" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Edited</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="last_edited" name="last_edited" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Edited By</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="edited_by" name="edited_by" readonly>
                    </div>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Login</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="last_login" name="last_login" readonly>
                    </div>
                  </div>
                </div>
                <center><br><br>
                  <button type="submit" class="btn btn-success" id="button_save">SAVE</button>
                </center>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              </div>
              <!-- /.card-footer -->
            </div>
            </form>
            <!-- /.card -->

            <!-- card -->
            <div class="card card-info text-sm">
              <div class="card-header">
                <h3 class="card-title">USER DATA</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row clearfix">
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm text-sm " id="searchby_nik_csdm" name="searchby_nik_csdm" placeholder="CSDM" autocomplete="off">
                  </div>
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm text-sm " id="searchby_name" name="searchby_name" placeholder="NAME" autocomplete="off">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary btn-sm waves-effect" id="button_filter"><i class="fa fa-search" style="color:white"></i></button>
                    <button type="button" class="btn btn-success btn-sm m-l-15 waves-effect" id="export">&nbsp;<i class='fas fa-file-excel'></i>&nbsp;</button>
                  </div>
                  <div class="col-sm-6">
                    <button type="button" class="btn btn-primary btn-sm m-l-15 waves-effect" id="button_create">CREATE NEW USER <i class='fas fa-plus'></i>&nbsp;</button>
                  </div>
                </div>
                <br>
                <table id="table_user" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>NIK CSDM</th>
                      <th>NAME</th>
                      <th>SITE</th>
                      <th>USER LEVEL</th>
                      <th>LAST LOGIN</th>
                      <th width="2">ACTION</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>NIK CSDM</th>
                      <th>NAME</th>
                      <th>SITE</th>
                      <th>USER LEVEL</th>
                      <th>LAST LOGIN</th>
                      <th>ACTION</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 