<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard Project</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<!-- /.content-header -->

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- card -->
            <div class="card card-danger card-outline" id="card_form">
              <div class="card-header">
                <h3 class="card-title" id="card_title">Update FORM</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body" id="project">
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">PROJECT</span>
                      </div>
                      <select class="form-control form-control-sm text-sm select2_project" id="id_project" name="id_project" required>
                        <option value="1">ACC TREAT</option>
                        <option value="2">SALES PLAN PROJECT</option>
                        <option value="3">EDUCATION PROJECT</option>
                        <option value="4">HEALTH PROJECT</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-2" style="visibility: hidden;">
                    <div class="input-group mb-3">
                      <button type="button" class="btn btn-danger btn-sm" id="button_load_data">Load</button>
                    </div>
                  </div>
                  <div class="col-sm-7" style="visibility: hidden;"><label>hide</label></div>
                </div>
                <div id="listproject"></div>
                <div class="form-group-sm row">
                  <div class="col-sm-2">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Month</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="month" name="month" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Active</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="total_active" name="total_active" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">revenue</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="total_revenue" name="total_revenue" readonly>
                    </div>
                  </div>
                  <!-- <div class="col-sm-4" style="visibility: hidden;"><label>hide</label></div> -->
                  <div class="col-sm-4" style="visibility: hidden;">
                    <input type="text" class="form-control form-control-sm text-sm" id="id_total" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Edited</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="last_edited" name="last_edited" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Edited By</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="edited_by" name="edited_by" readonly>
                    </div>
                  </div>
                </div>

              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- </form> -->
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 