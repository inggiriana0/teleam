<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard Summary</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<!-- /.content-header -->

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- card -->
            <div class="card card-success card-outline" id="card_form">
              <div class="card-header">
                <h3 class="card-title" id="card_title">Update FORM</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <form class="form-horizontal text-sm" id="summary_form">
                <input type="hidden" name="id" id="id" value="0" />
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">CALL</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="call" name="call" value="<?php echo $data['call'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">CONTACTED</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="contacted" name="contacted" value="<?php echo $data['contacted'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">AGREE</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="agree" name="agree" value="<?php echo $data['agree'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">AKTIF</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="aktif" name="aktif" value="<?php echo $data['aktif'];?>">
                    </div>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">TARGET EOY</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="target_eoy" name="target_eoy" value="<?php echo $data['target_eoy'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">REVENUE</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="revenue" name="revenue" value="<?php echo $data['revenue'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">GAP REV EOY</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="gap_rev_eoy" name="gap_rev_eoy" value="<?php echo $data['gap_rev_eoy'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">ACH EOY</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="ach_eoy" name="ach_eoy" value="<?php echo $data['ach_eoy'];?>">
                    </div>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <div class="col-sm-3" style="visibility: hidden;"><label>hide</label></div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">WEST REVENUE</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="west_revenue" name="west_revenue" value="<?php echo $data['west_revenue'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">EAST REVENUE</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="east_revenue" name="east_revenue" value="<?php echo $data['east_revenue'];?>">
                    </div>
                  </div>
                  <div class="col-sm-3" style="visibility: hidden;"><label>hide</label></div>
                </div>
                <br>
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Edited</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="last_edited" name="last_edited" value="<?php echo $data['last_edited'];?>" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Edited By</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="edited_by" name="edited_by" value="<?php echo $data['edited_by'];?>" readonly>
                    </div>
                  </div>
                </div>
                <center><br><br>
                  <button type="submit" class="btn btn-success" id="button_save">Save</button>
                </center>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              </div>
              <!-- /.card-footer -->
            </div>
            </form>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 