<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">DASHBOARD</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <!-- info-box -->
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>
              TOTAL CALL (<?php echo date('F Y'); ?>)
            </h3>
            <div class="row">
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-success elevation-1"><i class="fas fa-phone"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">SINGLE</span>
                    <span class="info-box-number">
                      <?php echo $data['total_single']; ?>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-blue elevation-1"><i class="fas fa-phone"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">BULK</span>
                    <span class="info-box-number">
                      <?php echo $data['total_bulk']; ?>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-red elevation-1"><i class="fas fa-phone"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">TAKERS</span>
                    <span class="info-box-number">
                      <?php echo $data['total_takers']; ?>
                    </span>
                  </div>
                </div>
              </div>
            </div> 
          </div>
        </div>
        <!-- /.info-box -->

        <!-- card: dashboard single -->
        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">SINGLE (<?php echo date('F Y'); ?>)</h3>
            <div class="card-tools">
              <!-- <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?php echo base_url()?>/C_admin/get_tes" data-source-selector="#card-refresh-content" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button> -->
              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>

          <!-- /.card-header -->
          <div class="card-body">
            <div class="row clearfix">
              <div class="col-sm-3 input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text form-control-sm text-sm">JENIS WL</span>
                </div>
                <select class="form-control form-control-sm text-sm select2_jenis_wl" id="single_jenis_wl" name="single_jenis_wl" data-placeholder="--- JENIS WL ---" required>
                  <option value="0">ALL</option>
                </select>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table m-0 table-bordered vtablecenter">
                  <tr>
                    <th rowspan="2">TOTAL CALL</th>
                    <th colspan="3">CONTACTED</th>
                    <th colspan="3">NOT CONTACTED</th>
                    <th rowspan="2">NOT CONTACTABLE</th>
                    <th rowspan="2">CONTACTED RATE</th>
                    <th rowspan="2">CONVERSION RATE</th>
                    <th rowspan="2">REVENUE</th>
                  </tr>
                  <tr>
                    <th>AGREE</th>
                    <th>DISAGREE</th>
                    <th>FOLLOW UP</th>
                    <th>BUSY</th>
                    <th>RNA</th>
                    <th>Voicemail / Tidak Aktif / di Luar Jangkauan</th>
                  </tr>
                  <!-- <tr>
                    <td><span class="badge badge-danger"><?php echo $data_single['total_call']; ?></span></td>
                    <td><span class="badge badge-success"><?php echo $data_single['agree']; ?></span></td>
                    <td><span class="badge badge-success"><?php echo $data_single['disagree']; ?></span></td>
                    <td><span class="badge badge-success"><?php echo $data_single['follow_up']; ?></span></td>
                    <td><span class="badge badge-success"><?php echo $data_single['busy']; ?></span></td>
                    <td><span class="badge badge-success"><?php echo $data_single['rna']; ?></span></td>
                    <td><span class="badge badge-success"><?php echo $data_single['voicemail']; ?></span></td>
                    <td rowspan="2"><span class="badge badge-danger"><?php echo $data_single['not_contactable']; ?></span></td>
                    <td rowspan="2"><span class="badge badge-danger"><?php echo $data_single['contacted_rate']; ?></span></td>
                    <td rowspan="2"><span class="badge badge-danger"><?php echo $data_single['conversion_rate']; ?></span></td>
                    <td rowspan="2"><span class="badge badge-danger"><?php echo $data_single['revenue']; ?></span></td>
                  </tr>
                  <tr>
                    <th>TOTAL</th>
                    <td colspan="3"><span class="badge badge-danger"><?php echo $data_single['contacted']; ?></span></td>
                    <td colspan="3"><span class="badge badge-danger"><?php echo $data_single['not_contacted']; ?></span></td>
                  </tr> -->
                  <tr>
                    <td><div id="single_total_call"><span class="badge badge-danger">0</span></div></td>
                    <td><div id="single_agree"><span class="badge badge-success">0</span></div></td>
                    <td><div id="single_disagree"><span class="badge badge-success">0</span></div></td>
                    <td><div id="single_follow_up"><span class="badge badge-success">0</span></div></td>
                    <td><div id="single_busy"><span class="badge badge-success">0</span></div></td>
                    <td><div id="single_rna"><span class="badge badge-success">0</span></div></td>
                    <td><div id="single_voicemail"><span class="badge badge-success">0</span></div></td>
                    <td rowspan="2"><div id="single_not_contactable"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="single_contacted_rate"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="single_conversion_rate"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="single_revenue"><span class="badge badge-danger">0</span></div></td>
                  </tr>
                  <tr>
                    <th>TOTAL</th>
                    <td colspan="3"><div id="single_contacted"><span class="badge badge-danger">0</span></div></td>
                    <td colspan="3"><div id="single_not_contacted"><span class="badge badge-danger">0</span></div></td>
                  </tr>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a> -->
            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> -->
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->

        <!-- card: dashboard bulk -->
        <div class="card card-blue">
          <div class="card-header">
            <h3 class="card-title">BULK (<?php echo date('F Y'); ?>)</h3>
            <div class="card-tools">
              <!-- <button type="button" id="card-refresh_widget" data-card-widget="card-refresh" data-source="dashboard/get_dashboard_bulk" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button> -->
              <!-- <button type="button" id="card_refresh_widgetbulk" class="btn btn-tool" data-card-widget="card-refresh" data-source="dashboard/get_dashboard_bulk" data-source-selector="#card-refresh-content" data-load-on-init="false"><i class="fas fa-sync-alt"></i></button> -->
              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>

          <!-- /.card-header -->
          <div class="card-body">
            <div class="row clearfix">
              <div class="col-sm-3 input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text form-control-sm text-sm">JENIS WL</span>
                </div>
                <!-- <select class="form-control form-control-sm text-sm select2_bulk_jenis_wl" id="bulk_jenis_wl" name="bulk_jenis_wl" data-placeholder="--- JENIS WL ---" required>
                  <option value="0">ALL</option>
                </select> -->
                <input type="text" class="form-control form-control-sm text-sm" id="bulk_jenis_wl" name="bulk_jenis_wl" value="ACC TREATMENT" readonly>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table m-0 table-bordered vtablecenter">
                <!-- <thead> -->
                  <tr>
                    <th rowspan="2">TOTAL CALL</th>
                    <th colspan="3">CONTACTED</th>
                    <th colspan="3">NOT CONTACTED</th>
                    <th rowspan="2">NOT CONTACTABLE</th>
                    <th rowspan="2">CONTACTED RATE</th>
                    <th rowspan="2">CONVERSION RATE</th>
                    <th rowspan="2">REVENUE</th>
                  </tr>
                  <tr>
                    <th>AGREE</th>
                    <th>DISAGREE</th>
                    <th>FOLLOW UP</th>
                    <th>BUSY</th>
                    <th>RNA</th>
                    <th>Voicemail / Tidak Aktif / di Luar Jangkauan</th>
                  </tr>
                <!-- </thead> -->
                <!-- <tbody> -->
                  <tr>
                    <td><div id="bulk_total_call"><span class="badge badge-danger">0</span></div></td>
                    <td><div id="bulk_agree"><span class="badge badge-success">0</span></div></td>
                    <td><div id="bulk_disagree"><span class="badge badge-success">0</span></div></td>
                    <td><div id="bulk_follow_up"><span class="badge badge-success">0</span></div></td>
                    <td><div id="bulk_busy"><span class="badge badge-success">0</span></div></td>
                    <td><div id="bulk_rna"><span class="badge badge-success">0</span></div></td>
                    <td><div id="bulk_voicemail"><span class="badge badge-success">0</span></div></td>
                    <td rowspan="2"><div id="bulk_not_contactable"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="bulk_contacted_rate"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="bulk_conversion_rate"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="bulk_revenue"><span class="badge badge-danger">0</span></div></td>
                  </tr>
                <!-- </tbody> -->
                <!-- <tfoot> -->
                  <tr>
                    <th>TOTAL</th>
                    <td colspan="3"><div id="bulk_contacted"><span class="badge badge-danger">0</span></div></td>
                    <td colspan="3"><div id="bulk_not_contacted"><span class="badge badge-danger">0</span></div></td>
                  </tr>
                <!-- </tfoot> -->
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a> -->
            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> -->
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->

        <!-- card: dashboard takers -->
        <div class="card card-red">
          <div class="card-header border-transparent">
            <h3 class="card-title">TAKERS (<?php echo date('F Y'); ?>)</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>

          <!-- /.card-header -->
          <div class="card-body">
            <div class="row clearfix">
              <div class="col-sm-3 input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text form-control-sm text-sm">JENIS WL</span>
                </div>
                <select class="form-control form-control-sm text-sm select2_takers_jenis_wl" id="takers_jenis_wl" name="takers_jenis_wl" data-placeholder="--- JENIS WL ---" required>
                  <option value="0">ALL</option>
                </select>
              </div>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table m-0 table-bordered vtablecenter">
                <!-- <thead> -->
                  <tr>
                    <th rowspan="2">TOTAL CALL</th>
                    <th colspan="3">CONTACTED</th>
                    <th colspan="3">NOT CONTACTED</th>
                    <th rowspan="2">NOT CONTACTABLE</th>
                    <th rowspan="2">CONTACTED RATE</th>
                    <th rowspan="2">CONVERSION RATE</th>
                    <th rowspan="2">REVENUE</th>
                  </tr>
                  <tr>
                    <th>AGREE</th>
                    <th>DISAGREE</th>
                    <th>FOLLOW UP</th>
                    <th>BUSY</th>
                    <th>RNA</th>
                    <th>Voicemail / Tidak Aktif / di Luar Jangkauan</th>
                  </tr>
                <!-- </thead> -->
                <!-- <tbody> -->
                  <tr>
                    <td><div id="takers_total_call"><span class="badge badge-danger">0</span></div></td>
                    <td><div id="takers_agree"><span class="badge badge-success">0</span></div></td>
                    <td><div id="takers_disagree"><span class="badge badge-success">0</span></div></td>
                    <td><div id="takers_follow_up"><span class="badge badge-success">0</span></div></td>
                    <td><div id="takers_busy"><span class="badge badge-success">0</span></div></td>
                    <td><div id="takers_rna"><span class="badge badge-success">0</span></div></td>
                    <td><div id="takers_voicemail"><span class="badge badge-success">0</span></div></td>
                    <td rowspan="2"><div id="takers_not_contactable"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="takers_contacted_rate"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="takers_conversion_rate"><span class="badge badge-danger">0</span></div></td>
                    <td rowspan="2"><div id="takers_revenue"><span class="badge badge-danger">0</span></div></td>
                  </tr>
                <!-- </tbody> -->
                <!-- <tfoot> -->
                  <tr>
                    <th>TOTAL</th>
                    <td colspan="3"><div id="takers_contacted"><span class="badge badge-danger">0</span></div></td>
                    <td colspan="3"><div id="takers_not_contacted"><span class="badge badge-danger">0</span></div></td>
                  </tr>
                <!-- </tfoot> -->
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a> -->
            <!-- <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> -->
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->