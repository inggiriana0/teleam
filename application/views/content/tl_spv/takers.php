<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">TAKERS</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<!-- /.content-header -->

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- card -->
            <div class="card card-info text-sm">
              <div class="card-header">
                <h3 class="card-title">TAKERS DATA</h3>
                <!-- <a href="#single_form" id="button_single_form">test</a> -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row clearfix">
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm text-sm " id="tgl_cari" name="tgl_cari" placeholder="TANGGAL" data-date-format="yyyy-mm-dd" autocomplete="off" value="<?php echo date('Y-m-d');?>">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary btn-sm waves-effect" id="button_filter"><i class="fa fa-search" style="color:white"></i></button>
                    <button type="button" class="btn btn-success btn-sm m-l-15 waves-effect" id="export">&nbsp;<i class='fas fa-file-excel'></i>&nbsp;</button>
                  </div>
                </div>
                <br>
                <table id="table_takers" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>DATE CREATED</th>
                      <th>MSISDN</th>
                      <th>JENIS WL</th>
                      <th>TANGGAL CALL</th>
                      <th>TANGGAL AKTIF</th>
                      <th>STATUS CALL</th>
                      <th width="2">ACTION</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>DATE CREATED</th>
                      <th>MSISDN</th>
                      <th>JENIS WL</th>
                      <th>TANGGAL CALL</th>
                      <th>TANGGAL AKTIF</th>
                      <th>STATUS CALL</th>
                      <th>ACTION</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- card -->
            <div class="card card-success" id="card_form" style="display: none;">
              <div class="card-header">
                <h3 class="card-title" id="card_title">VIEW DETAIL DATA</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <form class="form-horizontal text-sm" id="bulk_form">
                <input type="hidden" name="id_takers" id="id_takers" value="0" />
                <div class="form-group-sm row">
                  <label for="nik_csdm" class="col-sm-2 col-form-label">NIK CSDM</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nik_csdm" name="nik_csdm" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="name" class="col-sm-2 col-form-label">NAME</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="name" name="name" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="leader" class="col-sm-2 col-form-label">TEAM LEADER</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="leader" name="leader" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="site" class="col-sm-2 col-form-label">SITE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="site" name="site" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="jenis_wl" class="col-sm-2 col-form-label">JENIS WL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="jenis_wl" name="jenis_wl" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="tgl_call" class="col-sm-2 col-form-label">TANGGAL CALL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="tgl_call" name="tgl_call" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="jam_call" class="col-sm-2 col-form-label">JAM CALL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="jam_call" name="jam_call" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="tgl_aktif" class="col-sm-2 col-form-label">TANGGAL AKTIF</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="tgl_aktif" name="tgl_aktif" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="msisdn" class="col-sm-2 col-form-label">MSISDN</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="msisdn" name="msisdn" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="nama_pelanggan_dsc" class="col-sm-2 col-form-label">NAMA PELANGGAN (DSC)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nama_pelanggan_dsc" name="nama_pelanggan_dsc" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="nama_akun" class="col-sm-2 col-form-label">NAMA AKUN</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nama_akun" name="nama_akun" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="prt" class="col-sm-2 col-form-label">PRT</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="prt" name="prt" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="los" class="col-sm-2 col-form-label">LOS</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="los" name="los" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="arpu" class="col-sm-2 col-form-label">ARPU</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="arpu" name="arpu" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="status_call" class="col-sm-2 col-form-label">STATUS CALL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="status_call" name="status_call" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="reason_call" class="col-sm-2 col-form-label">REASON CALL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="reason_call" name="reason_call" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="subreason_call" class="col-sm-2 col-form-label">SUBREASON CALL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="subreason_call" name="subreason_call" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="paket_sebelumnya" class="col-sm-2 col-form-label">PAKET SEBELUMNYA</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="paket_sebelumnya" name="paket_sebelumnya" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="paket_sebelumnya" class="col-sm-2 col-form-label">PAKET PENAWARAN</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="paket_penawaran" name="paket_penawaran" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="revenue" class="col-sm-2 col-form-label">REVENUE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="revenue" name="revenue" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="revenue" class="col-sm-2 col-form-label">ESTIMASI REVENUE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="estimasi_revenue" name="estimasi_revenue" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="delta_revenue" class="col-sm-2 col-form-label">DELTA REVENUE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="delta_revenue" name="delta_revenue" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="wl_source" class="col-sm-2 col-form-label">WL SOURCE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="wl_source" name="wl_source" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="region" class="col-sm-2 col-form-label">REGION</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="region" name="region" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="area" class="col-sm-2 col-form-label">AREA</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="area" name="area" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="area" class="col-sm-2 col-form-label">PROJECT</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="project" name="project" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="keterangan" class="col-sm-2 col-form-label">KETERANGAN</label>
                  <div class="col-sm-10">
                    <textarea rows="4" class="form-control form-control-sm text-sm no-resize" name="keterangan" id="keterangan" readonly></textarea>
                  </div>
                </div>
                <br>
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Date Created</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="created" name="created" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Edited</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="last_edited" name="last_edited" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              </div>
              <!-- /.card-footer -->
            </div>
            </form>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 