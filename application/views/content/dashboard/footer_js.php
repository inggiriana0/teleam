<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- Bootstrap Datetimepicker Plugin Js -->
<script src="<?php echo base_url(); ?>assets/plugins/momentjs/2.27.0/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker_4.17.47/bootstrap-datetimepicker.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- jquery-mask-1.14.15 -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-mask-1.14.15/jquery.mask.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/plugins/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/chart.js/chartjs-plugin-datalabels.min.js"></script>

<script type="text/javascript">
    var base_url = '<?php echo base_url()?>';
    // money input
    // $('.money').mask('000.000.000.000.000.000', {reverse: true});
</script>

<?php
if (file_exists('assets/js/content/dashboard/' . $page . '.js')) {
?>
    <script src="<?php echo base_url(); ?>assets/js/content/dashboard/<?php echo $page; ?>.js"></script>
<?php
}
?>

