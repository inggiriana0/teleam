<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-12 justify-content-center">
			<div class="col-sm-12">
				<h3 class="m-0 text-dark bg-light text-center"> DASHBOARD TELE ACCOUNT MANAGEMENT </h1>
					<!-- <br> -->
			</div>
		</div>
		<div class="col-md-2 col-sm-6 col-12">
			<a href="<?php echo base_url() ?>dashboard/summary">
				<button type="button" id="button_menu_summary" class="btn btn-block btn-outline-success btn-sm">Summary</button>
			</a>
		</div>
		<div class="col-md-2 col-sm-6 col-12">
			<a href="<?php echo base_url() ?>dashboard/daily">
				<button type="button" id="button_menu_daily" class="btn btn-block btn-outline-primary btn-sm">Daily</button>
			</a>
		</div>
		<div class="col-md-2 col-sm-6 col-12">
			<a href="<?php echo base_url() ?>dashboard/monthly">
				<button type="button" class="btn btn-block btn-outline-warning btn-sm">Monthly</button>
			</a>
		</div>
		<div class="col-md-2 col-sm-6 col-12">
			<a href="<?php echo base_url() ?>dashboard/project">
				<button type="button" class="btn btn-block btn-outline-danger btn-sm">Project</button>
			</a>
		</div>
		<div class="col-md-2 col-sm-6 col-12">
			<a href="<?php echo base_url() ?>dashboard/region">
				<button type="button" class="btn btn-block btn-outline-secondary btn-sm">Region</button>
			</a>
		</div>
		<div class="col-md-2 col-sm-6 col-12">
			<a href="<?php echo base_url() ?>dashboard/best_performance">
				<button type="button" class="btn btn-block btn-outline-info btn-sm">Best Performance</button>
			</a>
		</div>
	</div>
</div>