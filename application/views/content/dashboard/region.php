  <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <div class="card card-secondary text-sm">
              <div class="card-header">
                <div class="d-flex justify-content-center">
	           	  <h3 class="card-title">RED SOCIETY</h3>
	            </div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
			  <div class="card-body p-0">
				<table id="table_region" class="vtablecenter table table-bordered table-striped text-sm table-d_region2">
	              <thead>
	               <!--  <tr>
	                  <th colspan="14" style="background: gray;">RED SOCIETY</th>
	                </tr> -->
	                <tr>
	                  <th style="width: 70px; background: powderblue;">Region</th>
	                  <th>Total</th>
	                  <th>Januari</th>
	                  <th>Februari</th>
	                  <th>Maret</th>
	                  <th>April</th>
	                  <th>Mei</th>
	                  <th>Juni</th>
	                  <th>Juli</th>
	                  <th>Agustus</th>
	                  <th>September</th>
	                  <th>Oktober</th>
	                  <th>November</th>
	                  <th>Desember</th>
	                </tr>
	              </thead>
	            </table>
              </div>
            </div>
          </div>

		  <div class="col-sm-12">
		    <div class="card card-secondary text-sm">
	          <div class="card-header">
	            <div class="d-flex justify-content-center">
	           	  <h3 class="card-title">EDUCATION</h3>
	            </div>
	            <div class="card-tools">
	              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
	              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
	            </div>
	          </div>
			  <div class="card-body p-0">
				<table id="table_region2" class="vtablecenter table table-bordered table-striped text-sm table-d_region2">
	              <thead>
	               <!--  <tr>
	                  <th colspan="14" style="background: gray;">RED SOCIETY</th>
	                </tr> -->
	                <tr>
	                  <th style="width: 70px; background: powderblue;">Region</th>
	                  <th>Total</th>
	                  <th>Januari</th>
	                  <th>Februari</th>
	                  <th>Maret</th>
	                  <th>April</th>
	                  <th>Mei</th>
	                  <th>Juni</th>
	                  <th>Juli</th>
	                  <th>Agustus</th>
	                  <th>September</th>
	                  <th>Oktober</th>
	                  <th>November</th>
	                  <th>Desember</th>
	                </tr>
	              </thead>
	            </table>
	          </div>
		    </div>
		  </div>

        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  <!-- /.content -->