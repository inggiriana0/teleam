  <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-lg-12">

            <!-- tabel card-->
               <div class="card">
                <div class="card-body p-0">
                   <div id="table-scroll" class="table-scroll table-autoscroll">
                    <table id="table_daily" class="table table-striped text-sm">
                      <thead>
                        <tr>
                          <th style="width: 70px">Date</th>
                          <th>Agent</th>
                          <th>Call</th>
                          <th>Contacted</th>
                          <th>Agree</th>
                          <th>Active</th>
                          <th>Revenue</th>
                          <th>Growth DoD</th>
                          <th>Contacted Rate</th>
                          <th>Conversion Rate</th>
                          <th>Active Rate</th>
                          <th>Target</th>
                          <th style="width: 50px">Ach</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Total</th>
                          <th><?php echo $data['agent'];?></th>
                          <th><?php echo $data['call'];?></th>
                          <th><?php echo $data['contacted'];?></th>
                          <th><?php echo $data['agree'];?></th>
                          <th><?php echo $data['active'];?></th>
                          <th><?php echo $data['revenue'];?></th>
                          <th><?php echo $data['growth_dod'];?></th>
                          <th><?php echo $data['contacted_rate'];?></th>
                          <th><?php echo $data['conversion_rate'];?></th>
                          <th><?php echo $data['active_rate'];?></th>
                          <th><?php echo $data['target'];?></th>
                          <th><span class="badge bg-primary"><?php echo $data['ach'];?></span></th>
                        </tr>
                      </tfoot>
                    </table>
                   </div>
                </div>
               </div>
            <!-- /.tabel card END-->

            <!-- mix CHART -->
              <div class="card">
                <div class="card-header border-0">
                  <div class="d-flex justify-content-center">
                    <h3 class="card-title">Revenue Achievement</h3>
                  </div>
                  <div class="card-tools d-flex justify-content-end">
                    <button type="button" class="btn btn-tool" title="Download" id="btn-download"><i class="fas fa-arrow-down"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="maximize" title="expand"><i class="fas fa-expand"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
              </div>
            <!-- /.mix CHART END-->

          </div>

        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  <!-- /.content -->