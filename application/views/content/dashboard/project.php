  <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-sm-3">
            <table id="table_acctreat" class="vtablecenter table table-bordered table-striped text-sm">
              <thead>
                <tr>
                  <th colspan="3" style="background: lime;">ACC TREAT</th>
                </tr>
                <tr>
                  <th style="width: 70px; background: powderblue;">Month</th>
                  <th>Active</th>
                  <th>Revenue</th>
                </tr>
              </thead>
            </table>
          </div>

          <div class="col-sm-3">
            <table id="table_salesplan" class="vtablecenter table table-bordered table-striped text-sm">
              <thead>
                <tr>
                  <th colspan="3" style="background: aqua;">SALES PLAN PROJECT</th>
                </tr>
                <tr>
                  <th style="width: 70px; background: powderblue;">Month</th>
                  <th>Active</th>
                  <th>Revenue</th>
                </tr>
              </thead>
            </table>
          </div>

          <div class="col-sm-3">
            <table id="table_education" class="vtablecenter table table-bordered table-striped text-sm">
              <thead>
                <tr>
                  <th colspan="3" style="background: yellow;">EDUCATION PROJECT</th>
                </tr>
                <tr>
                  <th style="width: 70px; background: powderblue;">Month</th>
                  <th>Active</th>
                  <th>Revenue</th>
                </tr>
              </thead>
            </table>
          </div>

          <div class="col-sm-3">
            <table id="table_health" class="vtablecenter table table-bordered table-striped text-sm">
              <thead>
                <tr>
                  <th colspan="3" style="background: #FC2C2C;">HEALTH PROJECT</th>
                </tr>
                <tr>
                  <th style="width: 70px; background: powderblue;">Month</th>
                  <th>Active</th>
                  <th>Revenue</th>
                </tr>
              </thead>
            </table>
          </div>

        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
  <!-- /.content -->