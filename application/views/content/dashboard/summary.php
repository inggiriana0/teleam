  <div class="content-wrapper">
  	<br>
  	<div class="row">

      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-phone-alt"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">CALL</span>
            <span class="info-box-number"><?php echo $data['call'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-blue elevation-1"><i class="fas fa-phone-volume"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">CONTACTED</span>
            <span class="info-box-number"><?php echo $data['contacted'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="fas fa-thumbs-up"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">AGREE</span>
            <span class="info-box-number"><?php echo $data['agree'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-yellow elevation-1"><i class="fas fa-laugh-beam"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">AKTIF</span>
            <span class="info-box-number"><?php echo $data['aktif'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-red elevation-1"><i class="ion ion-android-locate"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">TARGET EOY</span>
            <span class="info-box-number"><?php echo $data['target_eoy'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="ion ion-stats-bars"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">REVENUE</span>
            <span class="info-box-number"><?php echo $data['revenue'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-yellow elevation-1"><i class="fa fa-percent"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">GAP REV EOY</span>
            <span class="info-box-number"><?php echo $data['gap_rev_eoy'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="ion ion-ios-location"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">ACH EOY</span>
            <span class="info-box-number"><?php echo $data['ach_eoy'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-purple elevation-1"><i class="ion ion-android-arrow-back"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">WEST REVENUE</span>
            <span class="info-box-number"><?php echo $data['west_revenue'];?></span>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-blue elevation-1"><i class="ion ion-android-arrow-forward"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">EAST REVENUE</span>
            <span class="info-box-number"><?php echo $data['east_revenue'];?></span>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- /.content-wrapper -->