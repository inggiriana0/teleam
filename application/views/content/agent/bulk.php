<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">BULK</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<!-- /.content-header -->

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- card -->
            <div class="card card-primary" id="card_form">
              <div class="card-header">
                <h3 class="card-title" id="card_title">INPUT FORM</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <form class="form-horizontal text-sm" id="bulk_form">
                <input type="hidden" name="id_bulk" id="id_bulk" value="0" />
                <div class="form-group-sm row">
                  <label for="nik_csdm" class="col-sm-2 col-form-label">NIK CSDM</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nik_csdm" name="nik_csdm" placeholder="NIK CSDM" value="<?php echo $this->session->nik_csdm;?>" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="name" class="col-sm-2 col-form-label">NAME</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="name" name="name" placeholder="NAME" value="<?php echo $this->session->name;?>" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="leader" class="col-sm-2 col-form-label">TEAM LEADER</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="leader" name="leader" placeholder="TEAM LEADER" value="<?php echo $this->session->leader;?>" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="site" class="col-sm-2 col-form-label">SITE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="site" name="site" placeholder="SITE" value="<?php echo $this->session->site;?>" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="jenis_wl" class="col-sm-2 col-form-label">JENIS WL</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_jenis_wl" id="jenis_wl" name="jenis_wl" data-placeholder="--- JENIS WL ---" required>
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="tgl_call" class="col-sm-2 col-form-label">TANGGAL CALL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="tgl_call" name="tgl_call" placeholder="TANGGAL CALL" data-date-format="yyyy-mm-dd" autocomplete="off" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="jam_call" class="col-sm-2 col-form-label">JAM CALL</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_jam_call" id="jam_call" name="jam_call" data-placeholder="--- JAM CALL ---" required>
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="msisdn" class="col-sm-2 col-form-label">MSISDN</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control form-control-sm text-sm" id="msisdn" name="msisdn" placeholder="MSISDN : NUMBER (PREFIX 62)" required>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="nama_pelanggan_dsc" class="col-sm-2 col-form-label">NAMA PELANGGAN (DSC)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nama_pelanggan_dsc" name="nama_pelanggan_dsc" placeholder="NAMA PELANGGAN (DSC)">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="nama_akun" class="col-sm-2 col-form-label">NAMA AKUN</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nama_akun" name="nama_akun" placeholder="NAMA AKUN">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="prt" class="col-sm-2 col-form-label">PRT</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_prt" id="prt" name="prt" data-placeholder="--- PRT ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="los" class="col-sm-2 col-form-label">LOS</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control form-control-sm text-sm" id="los" name="los" placeholder="LOS">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="arpu" class="col-sm-2 col-form-label">ARPU</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_arpu" id="arpu" name="arpu" data-placeholder="--- ARPU ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="status_call" class="col-sm-2 col-form-label">STATUS CALL</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_status_call" id="status_call" name="status_call" data-placeholder="--- STATUS CALL ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="reason_call" class="col-sm-2 col-form-label">REASON CALL</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_reason_call" id="reason_call" name="reason_call" data-placeholder="--- REASON CALL ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="subreason_call" class="col-sm-2 col-form-label">SUBREASON CALL</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_subreason_call" id="subreason_call" name="subreason_call" data-placeholder="--- SUBREASON CALL ---">
                      <option></option>
                    </select>
                    <font color="#FA5858" size="2"><div id="ket_subreason_call" class=""></div></font>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="paket_sebelumnya" class="col-sm-2 col-form-label">PAKET SEBELUMNYA</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="paket_sebelumnya" name="paket_sebelumnya" placeholder="PAKET SEBELUMNYA">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="paket_penawaran" class="col-sm-2 col-form-label">PAKET PENAWARAN</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_paket_penawaran" id="paket_penawaran" name="paket_penawaran" data-placeholder="--- PAKET PENAWARAN ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="paket_aktif" class="col-sm-2 col-form-label">PAKET AKTIF</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_paket_aktif" id="paket_aktif" name="paket_aktif" data-placeholder="--- PAKET AKTIF ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="tgl_aktif" class="col-sm-2 col-form-label">TANGGAL AKTIF</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="tgl_aktif" name="tgl_aktif" placeholder="TANGGAL AKTIF" data-date-format="yyyy-mm-dd" autocomplete="off" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="revenue" class="col-sm-2 col-form-label">REVENUE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm money" id="revenue" name="revenue" placeholder="REVENUE">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="revenue" class="col-sm-2 col-form-label">ESTIMASI REVENUE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm money" id="estimasi_revenue" name="estimasi_revenue" placeholder="ESTIMASI REVENUE">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="delta_revenue" class="col-sm-2 col-form-label">DELTA REVENUE</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm money" id="delta_revenue" name="delta_revenue" placeholder="DELTA REVENUE">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="wl_source" class="col-sm-2 col-form-label">WL SOURCE</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_wl_source" id="wl_source" name="wl_source" data-placeholder="--- WL SOURCE ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="region" class="col-sm-2 col-form-label">REGION</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_region" id="region" name="region" data-placeholder="--- REGION ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="area" class="col-sm-2 col-form-label">AREA</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="area" name="area" placeholder="AREA" readonly>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="region" class="col-sm-2 col-form-label">CEK MEA</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_cek_mea" id="cek_mea" name="cek_mea" data-placeholder="--- CEK MEA ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="area" class="col-sm-2 col-form-label">NOMOR PKS</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nomor_pks" name="nomor_pks" placeholder="NOMOR PKS">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="region" class="col-sm-2 col-form-label">STAGGING PROGRESS</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_stagging_progress" id="stagging_progress" name="stagging_progress" data-placeholder="--- STAGGING PROGRESS ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="area" class="col-sm-2 col-form-label">NAMA PIC</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="nama_pic" name="nama_pic" placeholder="NAMA PIC">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="msisdn" class="col-sm-2 col-form-label">NOMOR HP PIC</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control form-control-sm text-sm" id="nomor_hp_pic" name="nomor_hp_pic" placeholder="NOMOR HP PIC : NUMBER (PREFIX 62)">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="area" class="col-sm-2 col-form-label">EMAIL PIC</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control form-control-sm text-sm" id="email_pic" name="email_pic" placeholder="EMAIL PIC">
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="region" class="col-sm-2 col-form-label">PROJECT</label>
                  <div class="col-sm-10">
                    <select class="form-control form-control-sm text-sm select2_project" id="project" name="project" data-placeholder="--- PROJECT ---">
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="form-group-sm row">
                  <label for="keterangan" class="col-sm-2 col-form-label">KETERANGAN</label>
                  <div class="col-sm-10">
                    <textarea rows="4" class="form-control form-control-sm text-sm no-resize" placeholder="KETERANGAN" name="keterangan" id="keterangan"></textarea>
                  </div>
                </div>
                <br>
              <div id="date_create_edit" style="display: none;">
                <div class="form-group-sm row">
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Date Created</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="created" name="created" readonly>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text form-control form-control-sm text-sm">Last Edited</span>
                      </div>
                      <input type="text" class="form-control form-control-sm text-sm" id="last_edited" name="last_edited" readonly>
                    </div>
                  </div>
                </div>
              </div>
                <center><br><br>
                  <button type="reset" class="btn btn-default float-left" id="reset_form">RESET FORM</button>
                  <button type="submit" class="btn btn-primary" id="button_save">SAVE</button>
                </center>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer">
                <center>
                  <button type="reset" class="btn btn-default float-left" id="reset_form">RESET FORM</button>
                  <button type="submit" class="btn btn-primary" id="button_save">SAVE</button>
                </center>
              </div> -->
              <!-- /.card-footer -->
            </div>
            </form>
            <!-- /.card -->

            <!-- card -->
            <div class="card card-info text-sm">
              <div class="card-header">
                <h3 class="card-title">BULK DATA</h3>
                <!-- <a href="#single_form" id="button_single_form">test</a> -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row clearfix">
                  <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm text-sm " id="tgl_cari" name="tgl_cari" placeholder="TANGGAL" data-date-format="yyyy-mm-dd" autocomplete="off" value="<?php echo date('Y-m-d');?>">
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-primary btn-sm waves-effect" id="button_filter"><i class="fa fa-search" style="color:white"></i></button>
                  </div>
                </div>
                <br>
                <table id="table_bulk" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>DATE CREATED</th>
                      <th>MSISDN</th>
                      <th>JENIS WL</th>
                      <th>TANGGAL CALL</th>
                      <th>TANGGAL AKTIF</th>
                      <th>STATUS CALL</th>
                      <th width="2">ACTION</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>DATE CREATED</th>
                      <th>MSISDN</th>
                      <th>JENIS WL</th>
                      <th>TANGGAL CALL</th>
                      <th>TANGGAL AKTIF</th>
                      <th>STATUS CALL</th>
                      <th>ACTION</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 