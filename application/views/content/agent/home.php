<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>
              TODAY (<?php echo date('d-m-Y'); ?>)
            </h3>

            <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3><?php echo $data['total_single_today']; ?></h3>
                    <p>SINGLE</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-android-checkmark-circle"></i>
                  </div>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                  <div class="inner">
                    <h3><?php echo $data['total_bulk_today']; ?></h3>
                    <p>BULK</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-android-checkmark-circle"></i>
                  </div>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-red">
                  <div class="inner">
                    <h3><?php echo $data['total_takers_today']; ?></h3>
                    <p>TAKERS</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-android-checkmark-circle"></i>
                  </div>
                </div>
              </div>
              <!-- ./col -->
            </div>
            <!-- /.Small boxes (Stat box)-->

          </div>
        </div>

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->