<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="brand-link">
      <img src="<?php echo base_url();?>assets/images/Infomedialogo_(2014).PNG" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">TANIA</span>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url();?>assets/images/user.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->name;?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?php echo base_url()?><?php echo $user_level;?>/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url()?><?php echo $user_level;?>/dashboard" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url()?>dashboard/summary" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard Full</p>
                </a>
              </li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    Dashboard data
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?php echo base_url()?><?php echo $user_level;?>/d_summary" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Summary</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url()?><?php echo $user_level;?>/d_daily" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Daily</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url()?><?php echo $user_level;?>/d_monthly" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Monthly</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url()?><?php echo $user_level;?>/d_project" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Project</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url()?><?php echo $user_level;?>/d_region" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Region</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url()?><?php echo $user_level;?>/d_bestperformance" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Best Performance</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                User
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url()?><?php echo $user_level;?>/user" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Data</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Forms
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url()?><?php echo $user_level;?>/single" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Single</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url()?><?php echo $user_level;?>/bulk" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url()?><?php echo $user_level;?>/takers" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Takers</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>c_login/do_log_out" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>