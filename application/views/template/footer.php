<!-- Main Footer -->
  <footer class="main-footer text-sm">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      <!-- Anything you want -->
    </div>
    <!-- Default to the left -->
    <strong>&copy; 2021 Infomedia ITCC<font color="#FF0000">BDG</font>.
  </footer>