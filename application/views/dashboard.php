<!DOCTYPE html>
<html lang="en">
<?php require_once('template/head.php') ?>

<body class="hold-transition layout-top-nav">
	<div class="wrapper">
	<!-- Navbar -->
	  	<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
	    	<?php require_once('content/dashboard/dashboard_title.php') ?>
	  	</nav>
	<!-- /.navbar -->

	<!-- Content Wrapper. Contains page content -->
  		<div class="content-wrapper">
			<?php require_once('content/dashboard/' . $page . '.php') ?>
		</div>
  	<!-- /.content-wrapper -->

		<?php require_once('template/footer.php') ?>
	</div>
	<?php require_once('content/dashboard/footer_js.php') ?>
</body>
</html>