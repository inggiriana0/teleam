<!DOCTYPE html>
<html lang="en">
<?php require_once('template/head.php') ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php require_once('template/navbar.php') ?>
  <?php require_once('template/'.$sidebar.'.php') ?>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php require_once('content/'.$user_level.'/'.$page.'.php') ?>
  </div>
  <!-- /.content-wrapper -->
  <?php require_once('template/right_sidebar.php') ?>
  <?php require_once('template/footer.php') ?>
</div>
<?php require_once('template/footer_js.php') ?>
</body>
</html>
